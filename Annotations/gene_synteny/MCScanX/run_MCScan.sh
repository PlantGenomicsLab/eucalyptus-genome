#!/bin/bash
#SBATCH --job-name=MCScan
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err



../MCScanX data/eu_eg

#../duplicate_gene_classifier data/eu_eg


#../downstream_analyses/detect_syntenic_tandem_arrays -g eu_eg.gff -b eu_eg.blast -s eu_eg.collinearity -o eu_eg.out

#../downstream_analyses/detect_collinear_tandem_arrays -g data/eu_eg.gff -b data/eu_eg.blast -c data/eu_eg.collinearity -o data/eu_eg_collinear_arrays.out

#../downstream_analyses/dissect_multiple_alignment -g data/eu_eg.gff -c data/eu_eg.collinearity -o data/eu_eg_multialign.out

#java dot_plotter -g data/eu_eg.gff -s data/eu_eg.collinearity -c dot.ctl -o data/dot.png


java dual_synteny_plotter -g data/eu_eg.gff -s data/eu_eg.collinearity -c dual_synteny.ctl -o data/dual_synteny.png

java circle_plotter -g data/eu_eg.gff -s data/eu_eg.collinearity -c circle.ctl -o data/circle.png

java bar_plotter -g data/eu_eg.gff -s data/eu_eg.collinearity -c bar.ctl -o data/bar.png


#../MCScanX_h data/eu_eg
