[[_TOC_]]

## **Structural Annotation**

This provides a guide for gene annotation with Illumina RNA-Seq data. In this study we used RNA-Seq data from the *E. grandis* v2.0 genome annotation for structural annotation of the *E. grandis* haplogenome. For the *E. urophylla* haplogenome, data of from the Mizrachi *et al.* 2017 GUxU3yo backcross population. 


#### 1) Read QC and trimming

The quality of RNA-Seq reads were checked with **FASTQC**. All reads were trimmed with **Trimmomatic** and only paired reads were used for genome annotation.

**Input:**

*	**trim.sh** script to trim reads
*	RNA-Seq reads in **fastq** format

**Output:**

*	Trimmed paired reads in **fastq** format


#### 2) Mapping RNA_Seq reads with HISAT

Trimmed and paired RNA-Seq reads will be mapped to the relevant masked haplogenome using **HISAT2**. 

The first step is to build an index of the ReapeatMasked haplogenomes:

`module load hisat2/2.1.0`
`hisat2-build -p 19 masked_genome.fasta genome_name_index`

Next the trimmed and paired RNA-Seq reads are aligned to the indexed haplogenome:

`hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x genome_name_index -1 ./path/to/file/trim_forward.fastq -2 ./path/to/file/trim_reverse.fastq -S file_name.sam`

Then convert the SAM file to binary format (BAM) using **samtools**:

`module load samtools/1.9`
`samtools sort -@ 40 file_name.sam | samtools view -bS -F 0x04 - > file_name.OnlyAligned.bam`

Finally, sort the BAM files and merge them into a single BAM file:

`samtools merge -nurlf -b bamlist.fofn allBamMerged.bam`

Here you will need a list of all bam files (**bamlist.fofn**)

**In summary, the following is needed:**

**Input:**

*	masked genome in **fasta** format
*	trimmed and paired reads in **fastq** format
*	**bamlist.fofn** file containing the names of all BAM alignment files for merging
*   **hisatFinal.sh** or **06_align.sh** script to run all commands

**Output:**

*	multiple **.bam** alignment files
*	single merged and sorted **.bam** file for gene prediction
*   the **.err** file generated should also contain simple statistics such as alignment rates of the RNA-Seq data


#### 3) Identifying and predicting genes with RNA-Seq data with BRAKER2

[**BRAKER2**](https://academic.oup.com/bioinformatics/article/32/5/767/1744611) will be used to identify and predict gene models based on our RNA-Seq data. A previous genome annotation can also be used to improve prediction models by provifing the protein fasta file from that annotation. BRAKER2 makes use of [**GeneMark**](http://opal.biology.gatech.edu/GeneMark/) as unsupervised machine learning process to produce gene models without needing RNA-Seq data. After this a supervised machine learning process is trained with gene models provided by GeneMark and the aligned RNA-Seq data. This is done by [**AUGUSTUS**](http://bioinf.uni-greifswald.de/augustus/). Before running BRAKER2, copy the AUGUSTUS executable path to our local directory to allow writing access to the config directory.

```
export TMPDIR=$homedir/tmp
GENOME=../path/to/file/genome.fa.masked
BAM=../path/to/merged/bam/finalbamfile.bam

braker.pl --genome=${GENOME} \
	--bam ${BAM} \
	--softmasking 1 \
    #--prot_seq=annotation_protein.faa \
    #--prg=gth --gth2traingenes \
	--gff3 \
	--cores 16
```

You also need to copy the GeneMark license key to your home directory. To do this, go to http://topaz.gatech.edu/GeneMark/license_download.cgi and select the ET version of GeneMark, fill in what is needed and get the download link. Using that link, use `wget` to download the licence to your home directory.


**Input:**

*   RepeatMasked genome file in **fasta** format
*   Merged final alignments in **bam** format (allBamMerged.bam)
*   **braker_1.sh** script to run braker with just RNA-Seq data as input OR 
*   **braker_prt_gen.sh** script to run braker with the previous genome annotation file
*   Previous genome annotation protein sequences in **fasta** format
*   **busco.sh** or **busco_prt.sh** script to give a summary of the initial genome annotation 

**Output:**

*   BRAKER2 generates a directory called braker with multiple output files. The following are of note:
	-	augustus.hints.gtf: Genes predicted by AUGUSTUS with hints from given extrinsic evidence. 
	-	augustus.hints.aa: Protein sequence files in FASTA-format
	-	augustus.hints.codingseq: Coding sequences in FASTA-format.
	-	GeneMark-E*/genemark.gtf: Genes predicted by GeneMark-ES/ET/EP/EP+ in GTF-format. This file will be missing if BRAKER was executed with proteins of close homology and the option --trainFromGth.
	-	braker.gtf: Union of augustus.hints.gtf and reliable GeneMark-EX predictions (genes fully supported by external evidence). In --esmode, this is the union of augustus.ab_initio.gtf and all GeneMark-ES genes. Thus, this set is generally more sensitive (more genes correctly predicted) and can be less specific (more false-positive predictions can be present).
	-	hintsfile.gff: The extrinsic evidence data extracted from RNAseq.bam and/or protein data.
*   A completeness assessment from **BUSCO**


#### 4) Quality control and filtering with gFACs and Interproscan

We use [**gFACS**](https://gitlab.com/PlantGenomicsLab/gFACs) to extract viable genes and proteins. To first test a baseline, we used gFACs with no filters to extract genes and proteins using the **gfacs_general.sh** script under the gfacs_general folder. This was performed on the BRAKER2 predictions based on RNA-Seq data alone. 

```
module load perl/5.32.1

genome="../final_masked.fasta"
alignment="../07_braker/braker/Sp_1/augustus.hints.gff3"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

if [ ! -d general ]; then
        mkdir general
fi

perl "$script" \

        -f braker_2.05_gff3 \
        --statistics \
        --splice-table \
        --get-protein-fasta \
        --fasta "$genome" \
        -O general \
        "$alignment"
```

Next we added filters to get the mono- and multi-exonic genes separately again only based on RNA-Seq data using the **gfacs.sh** script under the mono_multi_split folder.

```
hostname
date


module load perl/5.32.1

genome="../final_masked.fasta"
alignment="../07_braker/braker/Sp_1/augustus.hints.gff3"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"


if [ ! -d mono_o ]; then
        mkdir mono_o 
fi
if [ ! -d multi_o ]; then
        mkdir multi_o
fi

perl "$script" \
	-f braker_2.05_gff3 \
	--statistics \
	--statistics-at-every-step \
	--splice-table \
	--unique-genes-only \
	--rem-multiexonics \
	--rem-all-incompletes \
	--rem-genes-without-start-codon \
	--rem-genes-without-stop-codon \
	--get-protein-fasta \
	--fasta "$genome" \
	-O mono_o \
	"$alignment" 

perl "$script" \
	-f braker_2.05_gff3 \
	--statistics \
	--statistics-at-every-step \
	--splice-table \
	--unique-genes-only \
	--rem-monoexonics \
	--rem-5prime-3prime-incompletes \
	--rem-genes-without-start-and-stop-codon \
	--min-exon-size 6 \
	--get-protein-fasta \
	--fasta "$genome" \
	-O multi_o \
	"$alignment"
```

We did the same for the BRAKER2 predictions that included the previous genome annotation protein file (**gfacs_general_prt.sh** and **gfacs_prt.sh** scripts respectively). 

In addition also ran gFACs with the following filters (**gfacs_general_prt_f.sh**):

```
module load perl/5.32.1

genome="/path/to/file/genome.masked.fasta"
alignment="../07_braker/braker/Sp_5/augustus.hints.gff3"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

if [ ! -d general_prt_filtered ]; then
        mkdir general_prt_filtered
fi


perl "$script" \
	-f braker_2.05_gff3 \
	--statistics \
	--statistics-at-every-step \
	--splice-table \
	--unique-genes-only \
	--rem-5prime-3prime-incompletes \
	--rem-genes-without-start-and-stop-codon \
	--get-protein-fasta \
	--fasta "$genome" \
	-O general_prt_filtered \
	"$alignment"
```

Lastly, we combined multiple filters to get a final gene set by: 1) filtering for mono-exonic genes, 2) filtering for multi-exonic genes, 3) filtering mono-exonic genes for only those with protein domains and 4) combining the final mono-exonic and multi-exonic set (**gfacs_other.sh** script).

```
hostname
date


module load perl/5.32.1

genome="/path/to/file/genome_final_masked.fasta"
alignment="/core/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/gene_annotation/gra/hisat2/07_braker/braker/Sp_5/augustus.hints.gff3"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"


if [ ! -d mono_o_prt ]; then
        mkdir mono_o_prt 
fi
if [ ! -d multi_o_prt ]; then
        mkdir multi_o_prt
fi

perl "$script" \
	-f braker_2.05_gff3 \
	--statistics \
	--statistics-at-every-step \
	--splice-table \
	--unique-genes-only \
	--rem-multiexonics \
	--rem-all-incompletes \
	--rem-genes-without-start-codon \
	--rem-genes-without-stop-codon \
	--get-protein-fasta \
	--fasta "$genome" \
	-O mono_o_prt \
	"$alignment" 

perl "$script" \
	-f braker_2.05_gff3 \
	--statistics \
	--statistics-at-every-step \
	--splice-table \
	--unique-genes-only \
	--rem-monoexonics \
	--rem-5prime-3prime-incompletes \
	--rem-genes-without-start-and-stop-codon \
	--min-exon-size 6 \
	--get-protein-fasta \
	--fasta "$genome" \
	-O multi_o_prt \
	"$alignment"

date

cd mono_o_prt
if [ ! -d interproscan ]; then
        mkdir interproscan
fi
cd interproscan

echo "#!/bin/bash
#SBATCH --job-name=interproscan_pfam
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o interproscan_pfam_%j.out
#SBATCH -e interproscan_pfam_%j.err

module load interproscan

sed 's/*$//g' ../genes.fasta.faa > genes.fasta.faa

interproscan.sh -appl Pfam -i genes.fasta.faa" > interproscan_pfam.sh

sbatch --wait interproscan_pfam.sh

cd ../

sed 's/\s.*$//' interproscan/genes.fasta.faa.tsv | uniq > pfam_ids.txt

python ../filtergFACsGeneTable.py --table gene_table.txt --tablePath . --idList pfam_ids.txt$

cd ../multi_o_prt
# remove lines with 5_INC+3_INC here
grep '5_INC+3_INC' gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' > fullinc_ids.txt
grep 'gene' gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' > gene_table_ids.txt
grep -vxFf fullinc_ids.txt gene_table_ids.txt > comp_inc_ids.txt
python ../filtergFACsGeneTable.py --table gene_table.txt --tablePath . --idList comp_inc_ids.txt --idPath . --out comp_inc_gene_table.txt

if [ ! -d start_o ]; then
	mkdir start_o
fi

if [ ! -d stop_o ]; then
	mkdir stop_o
fi

alignment="comp_inc_gene_table.txt"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-genes-without-start-codon \
--fasta "$genome" \
-O start_o \
"$alignment"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--statistics-at-every-step \
--splice-table \
--rem-genes-without-stop-codon \
--fasta "$genome" \
-O stop_o \
"$alignment"

grep 'gene' start_o/gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' | uniq > starts_ids.txt
grep 'gene' stop_o/gene_table.txt | sed 's/^.*ID=/ID=/g' | sed 's/;.*$//g' |  uniq > stops_ids.txt
grep -vxFf starts_ids.txt comp_inc_ids.txt > missing_starts_ids.txt
grep -vxFf stops_ids.txt comp_inc_ids.txt > missing_stops_ids.txt
grep -xFf missing_starts_ids.txt missing_stops_ids.txt > missing_both_ids.txt
grep -vxFf missing_both_ids.txt comp_inc_ids.txt > completes_partials_ids.txt

python ../filtergFACsGeneTable.py --table comp_inc_gene_table.txt --tablePath . --idList completes_partials_ids.txt --idPath . --out completes_partials_gene_table.txt

cd ../

cat mono_o_prt/pfam_gene_table.txt  multi_o_prt/gene_table.txt > mono_multi_gene_table.txt

if [ ! -d final_o_prt ]; then
        mkdir final_o_prt
fi

alignment="mono_multi_gene_table.txt"

perl "$script" \
	-f gFACs_gene_table \
	--no-processing \
	--statistics \
	--splice-table \
	--get-protein-fasta \
	--create-gff3 \
	--create-gtf \
	--fasta "$genome" \
	-O final_o_prt \
	"$alignment"
```

For each of these, BUSCO was used to assess completeness.

To get to the final gene set to be functionally annotated:

**Input:**

*	The predicted protein alignment file from BRAKER2 in **gff3** format = **augustus.hint.gff3** 
*	Path to the gFACs.pl script
*	Masked genome in **fasta** format
*	**filtergFACsGeneTable.py** script

**Output:**

*	final_o_prt folder containing
	*	genes.fasta.faa - protein sequences in **fasta** format 
	*	gene_table.txt
	*	gFACs_log.txt 
	*	out.gff3
	*	out.gtf
	*	statistics.txt - final statistics
*	BUSCO completeness statistics


## **Functional annotation** 

#### 5) EnTap

Similar to above, functional Annotation with EnTap was performed for models based on 1) RNA-Seq data with no gFACs filtering only (**entap.sh**), 2) RNA-Seq data with the v2.0 annotation input and gFACs filtering (**09_entap_prtf.sh**) and 3) the RNA-Seq data with the v2.0 annotation input and INTERPROSCAN filtering (**09_entap_prt_ipscn.sh**) under the 4_entap/all_o folder. For 1 and 2, the split mono- and multi-exonic gene models were also functionally annotated with EnTap (found under the mono_o and multi_o folders).

For 3:

**Input:**

*	**09_entap_prt_ipscn.sh** script to run EnTap
*	**entap_config.txt** file to provide path to dependancies
*	Filtered final protein sequences from gFACs in **fasta** format
*	Pathway to relevant databases (-d flag)

`-d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.205.dmnd \`
`-d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd \`

**Output:**

*	A folder with the following subfolders and files:
```
├──	final_results --> final annotations
Gene ontology terms are normalized to levels based on the input flag from the user (or the default of 0,3,4). A level of 0 within the filename indicates that ALL GO terms will be printed to the annotation file. Normalization of GO terms to levels is generally done before enrichment analysis and is based upon the hierarchical setup of the Gene Ontology database.	
- final_annotations_lvlX.tsv: X represents the normalized GO terms for the annotation
- final_annotated.faa / .fnn: Nucleotide and protein fasta files containing all sequences that either hit databases through similarity searching or through the ontology stage
- final_unannotated.aa / .fnn: Nucleotide and protein fasta files containing all sequences that did not hit either through similarity searching nor through the ontology stage
├── ontology --> ontology results against EggNOG pipeline 
	├── EggNOG_DMND/
		├── processed/ --> contain annotations
			- annotated_sequences*.tsv/faa
├── similarity_search --> results from the diamond databases included in your search
	├── DIAMOND/  
		├── processed/ --> information based on best hits from similarity searches against databases 
			├──  complete/ 
			- best_hits_contam.faa/.fnn/.tsv will contain contaminants (protein/nucleotide) separated from the best hits file.
			- best_hits_no_contam.faa/.fnn/.tsv will contain sequences (protein/nucleotide) that were selected as best hits and not flagged as contaminants
			- no_hits.faa/.fnn/.tsv contain sequences (protein/nucleotide) from the transcriptome that did not hit against this particular database
			- unselected.tsv will contain result in several hits for each query sequence. With only one best hit being selected, the rest are unselected and end up here
└── transcriptomes
- **.log** file with a summary of the gene assignments and annotations
```

All resulting files can be found here: 
- `/core/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/gene_annotation/gra/hisat2`

- `/core/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/gene_annotation/uro/hisat2`
