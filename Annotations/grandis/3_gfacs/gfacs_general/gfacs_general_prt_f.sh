#!/bin/bash
#SBATCH --job-name=gfacs_general
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=40G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date


module load perl/5.32.1

genome="/core/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/gene_annotation/gra/hisat2/gra_final_masked.fasta"
alignment="/core/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/gene_annotation/gra/hisat2/07_braker/braker/Sp_5/augustus.hints.gff3"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

if [ ! -d general_prt_filtered ]; then
        mkdir general_prt_filtered
fi


perl "$script" \
	-f braker_2.05_gff3 \
	--statistics \
	--statistics-at-every-step \
	--splice-table \
	--unique-genes-only \
	--rem-5prime-3prime-incompletes \
	--rem-genes-without-start-and-stop-codon \
	--get-protein-fasta \
	--fasta "$genome" \
	-O general_prt_filtered \
	"$alignment"


