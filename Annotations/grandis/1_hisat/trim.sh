#!/bin/bash
#SBATCH --job-name=trim
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=60G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

cd ~/CBCNP/gene_annotation/gra/hisat2/RNASeq/flower/

module load Trimmomatic/0.39
java -jar $Trimmomatic PE -threads 8 -summary CACT.txt -validatePairs CACT_fastq_forward.fastq CACT_fastq_reverse.fastq \
	trim_CACT_fastq_forward.fastq singles_trim_CACT_fastq_forward.fastq \
	trim_CACT_fastq_reverse.fastq singles_trim_CACT_fastq_reverse.fastq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary GCTT.txt -validatePairs GCTT_fastq_forward.fastq GCTT_fastq_reverse.fastq \
	trim_GCTT_fastq_forward.fastq singles_trim_GCTT_fastq_forward.fastq \
	trim_GCTT_fastq_reverse.fastq singles_trim_GCTT_fastq_reverse.fastq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary TTGT.txt -validatePairs TTGT_fastq_forward.fastq TTGT_fastq_reverse.fastq \
	trim_TTGT_fastq_forward.fastq singles_trim_TTGT_fastq_forward.fastq \
	trim_TTGT_fastq_reverse.fastq singles_trim_TTGT_fastq_reverse.fastq \
	ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

cd ../immature_xylem/
java -jar $Trimmomatic PE -threads 8 -summary eu0004.txt -validatePairs eu0004_4_1.qseq.fastq eu0004_4_2.qseq.fastq \
        trim_eu0004_4_1.qseq.fastq singles_trim_eu0004_4_1.qseq.fastq \
        trim_eu0004_4_2.qseq.fastq singles_trim_eu0004_4_2.qseq.fastq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -phred64 -threads 8 -summary eu0006.txt -validatePairs eu0006_6_1.qseq.fastq eu0006_6_2.qseq.fastq \
        trim_eu0006_6_1.qseq.fastq singles_trim_eu0006_6_1.qseq.fastq \
        trim_eu0006_6_2.qseq.fastq singles_trim_eu0006_6_2.qseq.fastq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

cd ../matureleaf/
java -jar $Trimmomatic PE -threads 8 -summary CACT.txt -validatePairs CACT_fastq_forward.fastq CACT_fastq_reverse.fastq \
        trim_CACT_fastq_forward.fastq singles_trim_CACT_fastq_forward.fastq \
        trim_CACT_fastq_reverse.fastq singles_trim_CACT_fastq_reverse.fastq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary GCTT.txt -validatePairs GCTT_fastq_forward.fastq GCTT_fastq_reverse.fastq \
        trim_GCTT_fastq_forward.fastq singles_trim_GCTT_fastq_forward.fastq \
        trim_GCTT_fastq_reverse.fastq singles_trim_GCTT_fastq_reverse.fastq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30


cd ../phloem/
java -jar $Trimmomatic PE -threads 8 -summary GCTT.txt -validatePairs GCTT_fastq_forward.fastq GCTT_fastq_reverse.fastq \
        trim_GCTT_fastq_forward.fastq singles_trim_GCTT_fastq_forward.fastq \
        trim_GCTT_fastq_reverse.fastq singles_trim_GCTT_fastq_reverse.fastq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary TTGT.txt -validatePairs TTGT_fastq_forward.fastq TTGT_fastq_reverse.fastq \
        trim_TTGT_fastq_forward.fastq singles_trim_TTGT_fastq_forward.fastq \
        trim_TTGT_fastq_reverse.fastq singles_trim_TTGT_fastq_reverse.fastq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

cd ../roots/
java -jar $Trimmomatic PE -threads 8 -summary GC03Bsum.txt -validatePairs GC03B-1.fq GC03B-2.fq \
        trim_GC03B-1.fq singles_trim_GC03B-1.fq \
        trim_GC03B-2.fq singles_trim_GC03B-2.fq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary GC03_B2Asum.txt -validatePairs GC03_B2A-1.fq GC03_B2A-2.fq \
        trim_GC03_B2A-1.fq singles_trim_GC03_B2A-1.fq \
        trim_GC03_B2A-2.fq singles_trim_GC03_B2A-2.fq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary GC05_BR1Bsum.txt -validatePairs GC05_BR1B-1.fq GC05_BR1B-2.fq \
        trim_GC05_BR1B-1.fq singles_trim_GC05_BR1B-1.fq \
        trim_GC05_BR1B-2.fq singles_trim_GC05_BR1B-2.fq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

cd ../shoot_tips/
java -jar $Trimmomatic PE -threads 8 -summary CACT.txt -validatePairs CACT_fastq_forward.fastq CACT_fastq_reverse.fastq \
        trim_CACT_fastq_forward.fastq singles_trim_CACT_fastq_forward.fastq \
        trim_CACT_fastq_reverse.fastq singles_trim_CACT_fastq_reverse.fastq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -threads 8 -summary TTGT.txt -validatePairs TTGT_fastq_forward.fastq TTGT_fastq_reverse.fastq \
        trim_TTGT_fastq_forward.fastq singles_trim_TTGT_fastq_forward.fastq \
        trim_TTGT_fastq_reverse.fastq singles_trim_TTGT_fastq_reverse.fastq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

cd ../young_leaf/
java -jar $Trimmomatic PE -threads 8 -summary eu0003.txt -validatePairs eu0003_3_1.qseq.fastq eu0003_3_2.qseq.fastq \
        trim_eu0003_3_1.qseq.fastq singles_trim_eu0003_3_1.qseq.fastq \
        trim_eu0003_3_2.qseq.fastq singles_trim_eu0003_3_2.qseq.fastq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

java -jar $Trimmomatic PE -phred64 -threads 8 -summary eu0005.txt -validatePairs eu0005_5_1.qseq.fastq eu0005_5_2.qseq.fastq \
        trim_eu0005_5_1.qseq.fastq singles_trim_eu0005_5_1.qseq.fastq \
        trim_eu0005_5_2.qseq.fastq singles_trim_eu0005_5_2.qseq.fastq \
        ILLUMINACLIP:/isg/shared/apps/Trimmomatic/0.36/adapters/TruSeq3-PE-2.fa:2:30:10 \
        LEADING:30 TRAILING:30 SLIDINGWINDOW:4:20 MINLEN:30

