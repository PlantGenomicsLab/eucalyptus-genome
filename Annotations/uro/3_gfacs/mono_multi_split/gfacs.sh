#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=40G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date


module load perl/5.32.1

genome="/core/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/gene_annotation/uro/hisat2/uro_final_masked.fasta"
alignment="/core/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/gene_annotation/uro/hisat2/07_braker/braker/Sp_2/augustus.hints.gff3"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"


if [ ! -d mono_o ]; then
        mkdir mono_o 
fi
if [ ! -d multi_o ]; then
        mkdir multi_o
fi

perl "$script" \
	-f braker_2.05_gff3 \
	--statistics \
	--statistics-at-every-step \
	--splice-table \
	--unique-genes-only \
	--rem-multiexonics \
	--rem-all-incompletes \
	--rem-genes-without-start-codon \
	--rem-genes-without-stop-codon \
	--get-protein-fasta \
	--fasta "$genome" \
	-O mono_o \
	"$alignment" 

perl "$script" \
	-f braker_2.05_gff3 \
	--statistics \
	--statistics-at-every-step \
	--splice-table \
	--unique-genes-only \
	--rem-monoexonics \
	--rem-5prime-3prime-incompletes \
	--rem-genes-without-start-and-stop-codon \
	--min-exon-size 6 \
	--get-protein-fasta \
	--fasta "$genome" \
	-O multi_o \
	"$alignment"

date



