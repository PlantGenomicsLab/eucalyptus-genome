# Illumina read QC and trimming

This directory contains scripts related to assessment of Illumina read quality and trimming.

The first step is to run **FastQC** to check the quality of sequencing (**Illumina read QC**), after which reads can be trimmed with **Trimmomatic** and quality reevaluated with FastQC (**Illimuna read trimming**).

**Input:**

*	Raw Illumina short reads in FASTQ format
*	**Illumina read QC** script to check read quality with FASTQC

**Output:**

*	A FASTQC report file containing multiple read statistics (can be used to decide whether and what size to trim reads to)

It is also recommended at this stage to check for read contaminats using **Kraken2**.

After removal of contaminant reads, an estimate can be obtained from the unclassified Illumina short-reads using [**Jellyfish**](https://bioinformatics.uconn.edu/genome-size-estimation-tutorial/) to calculate a k-mer distribution for a specific k-mer size, and visualizing the output as a histogram with **GenomeScope**.

**Input:**

*	Raw Illumina short read files in FASTQ format
*	**jellyfish*.sh** script specifying the k-mer length for genome size estimate and transfrom the output file into a **.histo** file format for GenomeScope 

**Output:** 

*	21mer_out file for creation of .histo file
*	.histo file for input into GenomeScope

The .histo file can be uploaded onto the [GenomeScope](http://qb.cshl.edu/genomescope/) website and results viewed. 
