## **Gene based synteny**

This provides a guide for gene synteny comparison between two assembled genomes using [**MCScanX**](https://github.com/wyp1125/MCScanX) and [**MCScan-python**](https://github.com/tanghaibao/jcvi/wiki/MCscan-%28Python-version%29#workflow) version (which is part of the jcvi toolset).

### **MCScanX**

To run MCScanX, one first needs to blast the protein.fasta files agains one another. However, proteins that are in unscaffolded regions complicate things, so we first remove them. In addition we rename the gene ID's to make them specific for each haplotype.

##### **1) Getting the files ready**

Using the output fasta file from gfacs (named *genes.fasta.faa* and *out.gtf* and renamed per haplotype) we separate only the chromosome annotations (the expected file output is also given):

```
grep "^chr" eg.genes.gtf > eg.genes_onlyChr.gtf
grep "^chr" eu.genes.gtf > eu.genes_onlyChr.gtf
```

head -5 eg.genes_onlyChr.gtf
```
chr11   GFACS   gene    14511932        14513647        .       +       .       ID=g44512.1
chr11   GFACS   start_codon     14511932        14511934        .       +       .       .
chr11   GFACS   stop_codon      14513645        14513647        .       +       .       .
chr11   GFACS   CDS     14511932        14513647        .       +       .       Parent=g44512.t1
chr9    GFACS   gene    19675812        19676630        .       -       .       ID=g37111
```

Then to make the comparison easier to follow, we rename ID's based on the haplotype:

```
grep -P "\tgene\t" eu.genes_onlyChr.gtf | sed 's/ID=//;s/\;//' | sed "s/^chr/EU/" | awk '{print $1"\t"$1$9"\t"$4"\t"$5}' > eur_OnlyChr.gff
grep -P "\tgene\t" eg.genes_onlyChr.gtf | sed 's/ID=//;s/\;//' | sed "s/^chr/EG/" | awk '{print $1"\t"$1$9"\t"$4"\t"$5}' > egr_OnlyChr.gff
```

head egr_OnlyChr.gff
```
EG11    EG11g44512.1    14511932        14513647
EG9     EG9g37111       19675812        19676630
EG3     EG3g11022       28725694        28726020
EG5     EG5g20238       45094734        45095636
EG8     EG8g30216       3579554 3580033
```

```
grep -P "\tgene\t" eu.genes_onlyChr.gtf | sed 's/ID=//;s/\;//' | sed "s/^chr/EU/" | awk '{print $9"\t"$1$9}' > eu_renamed.map
grep -P "\tgene\t" eg.genes_onlyChr.gtf | sed 's/ID=//;s/\;//' | sed "s/^chr/EG/" | awk '{print $9"\t"$1$9}' > eg_renamed.map
```

head eg_renamed.map
```
g44512.1        EG11g44512.1
g37111  EG9g37111
g11022  EG3g11022
g20238  EG5g20238
g30216  EG8g30216
g33085  EG8g33085
g42827  EG10g42827
g12545  EG3g12545
g7483   EG2g7483
g11042  EG3g11042
```

```
grep '>' eg.genes.aa | sed 's/>//' > egr.only.1
grep '>' eu.genes.aa | sed 's/>//' > eur.only.1
```

head egr.only.1
```
ID=g44512.1
ID=g37111
ID=g11022
ID=g20238
ID=g30216
ID=g33085
ID=g42827
ID=g12545
ID=g7483
ID=g11042
```

We then use the [*faSomeRecords.py*](https://raw.githubusercontent.com/santiagosnchez/faSomeRecords/master/faSomeRecords.py) script to get the fasta sequences of the ID's above from the protein.fasta file

```
python3 faSomeRecords.py -f eg.genes.aa -l egr.only.1 --outfile egr.genes_only.1.faa
Found 39849 sequence(s)
Sequences saved to: egr.genes_only.1.faa
python3 faSomeRecords.py -f eu.genes.aa -l eur.only.1 --outfile eur.genes_only.1.faa
Found 37942 sequence(s)
Sequences saved to: eur.genes_only.1.faa
```

We change the ID= in the *.genes_only.1.faa* file to just g with sed
```
sed -i 's/ID=//g' eur.genes_only.1.faa
sed -i 's/ID=//g' egr.genes_only.1.faa
```

And rename g### to EG#g### with maker
```
module load maker/2.31.9
map_fasta_ids eg_renamed.map egr.genes_only.1.faa
map_fasta_ids eu_renamed.map eur.genes_only.1.faa
grep '>' eur.genes_only.1.faa | grep ">EU" | sed 's/>//' > EUOnlyChrGeneID
grep '>' egr.genes_only.1.faa | grep ">EG" | sed 's/>//' > EGOnlyChrGeneID
```

Again we use *faSomeRecords.py* to rename the fasta sequences:
```
python3 faSomeRecords.py -f egr.genes_only.1.faa -l EGOnlyChrGeneID --outfile eg.protein.faa
Found 36676 sequence(s)
Sequences saved to: eg.protein.faa
python3 faSomeRecords.py -f eur.genes_only.1.faa -l EUOnlyChrGeneID --outfile eu.protein.faa
Found 35532 sequence(s)
Sequences saved to: eu.protein.faa
```

head eg.protein.faa
```
>EG11g44512.1
MPFFNPRKSRPLPLQKRQTTMNCRVLLFMAIVMFLAIRFIEETRVINHKNRNFNASNSGGIESLLNPSFGSVPSSGSTPGSITSPLSETKQEGGDRLGYGGGDFTYADDGETVELRVSHRSTSGGTGQEESFAAEDLARAQTDHGKMAEIRHRNPISRLSRGKDKLKLHLKPIDGSPSQESVAGGPPGTLMATLRYGVVPGLEVYFIDVYIGTPPRHVPLILDTGSDFSWIKCAACPDCSEQGGPYYDPERSTSFREISCRDPRCRHVRDPDPLQRCKSENQTCPYIYPYGDGSNSTGNLALETFTVNLTAQAGAPEFTRKEDVMFGCGRWNGGESDGAPGMLGLGRGPLSLASQLQSPNGQSFSYCLANRHSNPNASSRLVFGEANELSSRPGWNFTSFLRAEESPDSTFYYVKIKSIMVGGEILDIPQETWEVSPDGRGGTIIDSGVSLGYFFGPAYRAILEAFERKVKAYPMVEKEDFYPCYNVSGVPEPELPRFGIEFVDGAKWNFPLENNFVLFEPAGIACLAMMEQGGFPGYSIIGNYLQQDFHMWFDMKKSRLGFAPRKCADDV*
>EG9g37111
MGSLENGSDEPLTPIGRLFLQKEMNQVIYCAFGMRQPLDVDALKLAIQNCPMFMHPRFCSLLVRDSAGREHWRRTRVDIDRHVILVHGPIPEGLSDEAAVNEYLANLSTDLPGLMGDDKPLWDVHLLMAHQCMVLRIHHALGDGISLISSFLGGSRRVDDPEAVPTIGPPSSTRRRRSDGGGEWWKAVVRVLEVVWFTVVFVVELVLRIMWVSDRTTPLSGGAGVELWPRKLATARFSLEDMKVVKSAVANAVSSMCLSPKKKNSTADSILS*
```


##### **2) Blasting the proteins**

 The protein sequences are blasted against each other to get gene orthologs.

** Input:**

*	Protein sequences in **.fasta/.faa** format
*	**blast_*.sh** script to create blast databases of the fasta files and get the matches


** Output:**

*	blast database
*	**.blast** file containing tsv format blast hits for the top 5 alignments


##### **3) Running MCScanX**
To run MCScanX, all the blast results should be in one blast table. So we concatenate the resulting single blast outputs into one file with the self blast results and the *E. grandis* vs *E. urophylla* blast results.


** Input:**

*	Concatenated blast output file in **.tsv** format with correct name (eu_eg in this case, so MCScan will look at all files with this name)
*	Concatenated **.gff** file of all EG and EU gene positions for plotting
*	**MCScan_*.sh** file to run MCScanX and for plotting results
*	The **.ctl** files for plotting results. Plotting also requires the **.collinearity** output file of MCScanX


** Output:**

*	**.collinearity** file used for making plots. It also contains the alignment blocks so the size of the block in terms of number of genes can be extracted from here

head -35 eg_eu.collinearity
```
############### Parameters ###############
# MATCH_SCORE: 50
# MATCH_SIZE: 5
# GAP_PENALTY: -1
# OVERLAP_WINDOW: 5
# E_VALUE: 1e-05
# MAX GAPS: 25
############### Statistics ###############
# Number of collinear genes: 44369, Percentage: 61.45
# Number of all genes: 72208
##########################################
## Alignment 0: score=423.0 e_value=3.4e-18 N=9 EG1&EG1 plus
  0-  0:        EG1g539 EG1g587  1e-180
  0-  1:        EG1g542.1       EG1g591  8e-164
  0-  2:        EG1g545 EG1g599   8e-34
  0-  3:        EG1g546 EG1g601   2e-51
  0-  4:        EG1g547 EG1g604  1e-139
  0-  5:        EG1g548 EG1g607   2e-29
  0-  6:        EG1g550 EG1g617   1e-95
  0-  7:        EG1g553 EG1g619   5e-49
  0-  8:        EG1g561 EG1g623   2e-25
```

*	**.png** images of plots specified in the script (circle, bar, dot and dual_synteny plots)
*	**html** folder with visual alignment (in terms of gene ID) per chromosome

### **MCScan_py**

To use MCScan_py, we again get the files ready like above (step 1) which we then use as input for running MCScan_py.

**Input:**

* **MCScan_py_cXX.sh** script to run MCScan_py with the specified Cscore. The higher the Cscore, the more closely related we expect the species to be and the greater the filter for identity between the two gene annotations.
* **blocks.layout** file containing that tells the plotter what to draw, where to draw and what .bed files to use
* **seqids** file to specify which chromosomes to plot and what order
* **layout** file to specify labels, their position and colour

**Output:**

* **.anchors** file containing last alignments (synteny blocks) of gene or protein sequences which can be filtered using Cscore parameter for all remaining plots
* A dotplot alignment of gene pairs (default shows 10 000 random alignments but can be altered to show more) 
* A historgram showing the synteny pattern (1:1 in this case)
* **.simple** file which is a Cscore filtered anchors file with synteny blocks (can change minspan parameter to set the minimum gene block size). This file can be altered to change the colour of specific synteny blocks.
* A macrosynteny plot
