#!/bin/bash
#SBATCH --job-name=MCScan
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

. ~/miniconda3/etc/profile.d/conda.sh
conda activate last_jcvi


#Convert the GFF to BED file and rename them.
python -m jcvi.formats.gff bed --type=mRNA eg_hap.gene.gff3 -o grandis.bed
python -m jcvi.formats.gff bed --type=mRNA eu_hap.gene.gff3 -o urophylla.bed
python -m jcvi.formats.gff bed --type=mRNA --key=Name Egrandis_297_v2.0.gene.gff3 -o eg2.bed

#Reformat phytozome fasta
python -m jcvi.formats.fasta format eg_hap.cds.fa grandis.cds
python -m jcvi.formats.fasta format eu_hap.cds.fa urophylla.cds
python -m jcvi.formats.fasta format Egrandis_297_v2.0.cds.fa eg2.cds


#Make grandis primary for comparison
#python -m jcvi.formats.gff bed --type=mRNA --key=Name --primary_only eg_hap.gene.gff3 -o grandis.bed
#python -m jcvi.formats.gff bed --type=mRNA --key=Name --primary_only eu_hap.gene.gff3 -o urophylla.bed


#Do last comparison
#lastdb urophylla urophylla.cds
#lastal -u 0 -i3G -f BlastTab urophylla grandis.cds > ./grandis.urophylla.last
#python -m jcvi.compara.catalog ortholog grandis urophylla --no_strip_names

module load texlive
python -m jcvi.compara.catalog ortholog grandis eg2 --cscore=.99 --no_strip_names
python -m jcvi.compara.catalog ortholog eg2 urophylla --cscore=.99 --no_strip_names
python -m jcvi.compara.catalog ortholog grandis urophylla --cscore=.99 --no_strip_names


#Pairwise synteny dotplot
#python -m jcvi.graphics.dotplot grandis.urophylla.anchors
python -m jcvi.graphics.dotplot grandis.eg2.anchors

#Synteny pattern check
#python -m jcvi.compara.synteny depth --histogram grandis.urophylla.anchors
python -m jcvi.compara.synteny depth --histogram grandis.eg2.anchors

#Macrosynteny visualization
python -m jcvi.compara.synteny screen --minspan=30 --simple grandis.eg2.anchors grandis.eg2.anchors.new
python -m jcvi.compara.synteny screen --minspan=30 --simple eg2.urophylla.anchors eg2.urophylla.anchors.new
python -m jcvi.graphics.karyotype seqids layout


#Microsynteny
python -m jcvi.compara.synteny mcscan grandis.bed grandis.eg2.lifted.anchors --iter=1 -o grandis.eg2.i1.blocks


#Make plot of synteny
cat grandis.bed eg2.bed > grandis_eg2.bed
python -m jcvi.graphics.synteny blocks.layout grandis_eg2.bed blocks.layout --glyphcolor=orthogroup

#head -50 grandis.eg2.i1.blocks > blocks
#python -m jcvi.graphics.synteny blocks grandis_eg2.bed blocks.layout --glyphcolor=orthogroup
