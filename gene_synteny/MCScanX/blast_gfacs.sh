#!/bin/bash
#SBATCH --job-name=blastdb
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


module load blast/2.7.1

# make protein blast database
makeblastdb -in eg.protein.faa -dbtype prot -parse_seqids -out eg
makeblastdb -in eu.protein.faa -dbtype prot -parse_seqids -out eu


blastp -query eg.protein.faa -db eg -out eg_self.blast -evalue 1e-10 -num_threads 16 -outfmt 6 -num_alignments 5
blastp -query eu.protein.faa -db eu -out eu_self.blast -evalue 1e-10 -num_threads 16 -outfmt 6 -num_alignments 5

blastp -query eu.protein.faa -db eg -out db_eg_q_eu.blast -evalue 1e-10 -num_threads 16 -outfmt 6 -num_alignments 5
blastp -query eg.protein.faa -db eu -out db_eu_q_eg.blast -evalue 1e-10 -num_threads 16 -outfmt 6 -num_alignments 5
