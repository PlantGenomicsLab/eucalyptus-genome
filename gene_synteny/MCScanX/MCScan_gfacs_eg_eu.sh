#!/bin/bash
#SBATCH --job-name=MCScan
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err



../MCScanX data/eg_eu

../duplicate_gene_classifier data/eg_eu

../downstream_analyses/detect_syntenic_tandem_arrays -g data/eg_eu.gff -b data/eg_eu.blast -s data/eg_eu.collinearity -o data/eg_eu.out

../downstream_analyses/detect_collinear_tandem_arrays -g data/eg_eu.gff -b data/eg_eu.blast -c data/eg_eu.collinearity -o data/eg_eu_collinear_arrays.out

../downstream_analyses/dissect_multiple_alignment -g data/eg_eu.gff -c data/eg_eu.collinearity -o data/eg_eu_multialign.out

java dot_plotter -g data/eg_eu.gff -s data/eg_eu.collinearity -c dot.ctl -o data/dot_eg_eu.png

java dual_synteny_plotter -g data/eg_eu.gff -s data/eg_eu.collinearity -c dual_synteny.ctl -o data/dual_synteny_eg_eu.png

java circle_plotter -g data/eg_eu.gff -s data/eg_eu.collinearity -c circle.ctl -o data/circle_eg_eu.png

java bar_plotter -g data/eg_eu.gff -s data/eg_eu.collinearity -c bar.ctl -o data/bar_eg_eu.png


#../MCScanX_h data/eg_eu
