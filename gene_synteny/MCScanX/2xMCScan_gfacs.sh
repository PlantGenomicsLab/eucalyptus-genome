#!/bin/bash
#SBATCH --job-name=MCScan
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err



../MCScanX 2xdata/eu

../duplicate_gene_classifier 2xdata/eu

../downstream_analyses/detect_syntenic_tandem_arrays -g 2xdata/eu.gff -b 2xdata/eu.blast -s 2xdata/eu.collinearity -o 2xdata/eu.out

../downstream_analyses/detect_collinear_tandem_arrays -g 2xdata/eu.gff -b 2xdata/eu.blast -c 2xdata/eu.collinearity -o 2xdata/eu_collinear_arrays.out

../downstream_analyses/dissect_multiple_alignment -g 2xdata/eu.gff -c 2xdata/eu.collinearity -o 2xdata/eu_multialign.out

java dot_plotter -g 2xdata/eu.gff -s 2xdata/eu.collinearity -c dot.ctl -o 2xdata/dot_eu.png

java dual_synteny_plotter -g 2xdata/eu.gff -s 2xdata/eu.collinearity -c dual_synteny.ctl -o 2xdata/dual_synteny_eu.png

java circle_plotter -g 2xdata/eu.gff -s 2xdata/eu.collinearity -c circle.ctl -o 2xdata/circle_eu.png

java bar_plotter -g 2xdata/eu.gff -s 2xdata/eu.collinearity -c bar.ctl -o 2xdata/bar_eu.png


../MCScanX 2xdata/eg

../duplicate_gene_classifier 2xdata/eg

../downstream_analyses/detect_syntenic_tandem_arrays -g 2xdata/eg.gff -b 2xdata/eg.blast -s 2xdata/eg.collinearity -o 2xdata/eg.out

../downstream_analyses/detect_collinear_tandem_arrays -g 2xdata/eg.gff -b 2xdata/eg.blast -c 2xdata/eg.collinearity -o 2xdata/eg_collinear_arrays.out

../downstream_analyses/dissect_multiple_alignment -g 2xdata/eg.gff -c 2xdata/eg.collinearity -o 2xdata/eg_multialign.out

java dot_plotter -g 2xdata/eg.gff -s 2xdata/eg.collinearity -c dot.ctl -o 2xdata/dot_eg.png

java dual_synteny_plotter -g 2xdata/eg.gff -s 2xdata/eg.collinearity -c dual_synteny.ctl -o 2xdata/dual_synteny_eg.png

java circle_plotter -g 2xdata/eg.gff -s 2xdata/eg.collinearity -c circle.ctl -o 2xdata/circle_eg.png

java bar_plotter -g 2xdata/eg.gff -s 2xdata/eg.collinearity -c bar.ctl -o 2xdata/bar_eg.png

