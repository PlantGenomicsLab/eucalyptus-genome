#!/bin/bash
#SBATCH --job-name=canu_assembly
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=450G
#SBATCH --mail-user=mail.user@uconn.edu
#SBATCH -o /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Canu_assembly/%x_%j.out
#SBATCH -e /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Canu_assembly/%x_%j.err

#module load gnuplot/5.2.2

module load canu/1.8

canu -p Canu_AllData_1000 -d /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Canu_assembly/ genomeSize=650m \
        -minReadLength=1000 \
        -gridOptions="--partition=himem2 --qos=himem --mem-per-cpu=8032m --cpus-per-task=16" canuIteration=1 \
        -haplotypeURO /projects/EBP/CBC/eucalyptus/rawReads/Illumina/URO_FK1556_* \
        -haplotypeGRA /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_* \
        -nanopore-raw /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/GUF1_PoreChopped_MinIONandPromethION.fastq
