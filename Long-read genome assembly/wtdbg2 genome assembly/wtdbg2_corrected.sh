#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=8:00:00
#PBS -q normal
#PBS -o /nlustre/users/anneri/CombinedONT/wtdbg/wtdbg2_stdout.log
#PBS -e /nlustre/users/anneri/CombinedONT/wtdbg/wtdbg2_stderr.log
#PBS -k oe
#PBS -m abe
#PBS -M anneri.lotter@fabi.up.ac.za

#load module
module load wtdbg2

wtdbg2 -x ont -g 650m -t 24 -e 2 \
       -i /nlustre/users/anneri/CombinedONT/GUF1_PoreChopped_MinIONandPromethION.fastq \
       -o /nlustre/users/anneri/CombinedONT/wtdbg/GUF1_Combined_Nanopore_wtdbg.fa

wtpoa-cns -t 16 -i /nlustre/users/anneri/CombinedONT/wtdbg/GUF1_Combined_Nanopore_wtdbg.fa.ctg.lay.gz -fo /nlustre/users/anneri/CombinedONT/wtdbg/EucGUF1_wtdbg2assembly.raw.fa

module load abyss
abyss-fac /nlustre/users/anneri/CombinedONT/wtdbg/EucGUF1_wtdbg2assembly.raw.fa >> ../abyss_combinedData.txt

#cd /nlustre/users/anneri/EucGUF1_MinIONruns/PromethION/wtdbg2/
#module load minimap2
#minimap2 -t28 -ax map-ont /nlustre/users/anneri/EucGUF1_MinIONruns/PromethION/wtdbg2/wtdbg2_CombinedNanopore.fa /nlustre/users/anneri/EucGUF1_MinIONruns/PromethION/GUF1_NanoporeCombined_porechoppedreads.fastq > EucGUF1_dbg.sam

#module load samtools-1.7
#samtools sort -@4 >dbg.bam
#samtools view -F0x900 dbg.bam
#samtools view -S -b EucGUF1_dbg.sam > wtdbg2.bam
#samtools sort wtdbg2.bam -o wtdbg2.sorted.bam
#samtools index wtdbg2.sorted.bam

#module load bedtools2
#bedtools bedtobam -i wtdbg2.sorted.bam > wtdbg2_bedtobam.bed
#sort -k 1,1 wtdbg2_bedtobam.bed > wtdbg2_bedtobam.bed.sorted
#genomeCoverageBed -i wtdbg2_bedtobam.bed.sorted -g genome.txt -bg wtdbg2.bedGraph