#!/bin/bash
#SBATCH --job-name=quast_long
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=30G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/flye/Flye_Rawreads_ONT/%x_%A.out
#SBATCH -e /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/flye/Flye_Rawreads_ONT/%x_%A.err

hostname
module load quast/5.0.2

# flye statistics
quast.py /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/flye/Flye_Rawreads_ONT/assembly.fasta -o Flye_Quast
