#!/bin/bash
#SBATCH --job-name=falc
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o falcon_%j.out
#SBATCH -e falcon_%j.err

. ~/miniconda3/etc/profile.d/conda.sh

conda activate denovo_py3
conda install pb-assembly

fc_run fc_run.cfg

#fc_unzip.py fc_unzip.cfg
