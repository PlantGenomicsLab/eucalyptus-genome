# Visualization of genomic elements with CIRCOS

This folder contains files needed to run create CIRCOS plots.

**Input:**
*   **.txt** file specifying the karyotype of the genome to be visualised. This file will have the following information in the following format:
    chr - ID LABEL START END COLOUR
*   **.txt** file specifying the labels and their position
*   **.conf** file (configuration file). This will be used to specify what type of graph must be drawn at which position in the circos plot i.e. a histogram from 0.89r to 0.79r.
*   A file containing the information to be plotted. This may be a bed file but this file should contain the following: 
    Chr START END Value
    The value is what will be the histogram value.
    
**Output:**
*   **.png** and **.svg** files containing the circos image

To run circos (on zoidberg) use the following command:
*      /localextradata/Circos/circos-0.62-1/bin/circos -conf Anneri_assembly_coverage2.conf -debug_group textplace