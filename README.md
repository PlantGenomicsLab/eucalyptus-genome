[[_TOC_]]

# F1 _Eucalyptus grandis_ x _E. urophylla_

NCBI BioProject: [PRJNA885070](https://www.ncbi.nlm.nih.gov/bioproject/885070)\
Data: /archive/labs/wegrzyn/genomes/eucalyptus/2019_MSc\
Illumina PE 150bp: 178.6X coverage\
Nanopore: 105.4X\
Size estimation in GenomeScope - 425,203,321 bp

## _Eucalyptus urophylla_ maternal 

NCBI BioProject: [PRJNA874516](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA874516/)\
Data: /archive/labs/wegrzyn/genomes/eucalyptus/2019_MSc\
Illumina PE 150bp: 196.2X coverage\
Nanopore: 50X coverage\
Size estimation in GenomeScope - 412,295,044 bp

## _Eucalyptus grandis_ paternal

NCBI BioProject: [PRJNA870669](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA870669/)\
Data: /archive/labs/wegrzyn/genomes/eucalyptus/2019_MSc\
Illumina PE 150bp: 217.8X coverage\
Nanopore: 54X coverage\
Size estimation in GenomeScope - 428,673,422bp

# *Eucalyptus* Genome

Repository for scripts related to the *Eucalyptus* genome analysis. Make note of the bash script headers as some jobs were run on different cluster systems.

This [spreadsheet](https://docs.google.com/spreadsheets/d/1kgCXL3GB2o9xHkLu86ccIHauoIPRVLDWrz_ak5pV75w/edit?usp=sharing) contains all raw outputs and summary statistics.


## **Project background**

Current breeding strategies for eucalypt tree improvement are less accurate than haplotype-based molecular breeding strategies being deployed in other crop species. However, to use haplotype-based molecular breeding strategies for tree improvement, a high-quality reference genome is required for the species. The current *E. grandis* genome has been assembled using short-read technologies, which pose a variety of challenges and result in fragmented and incomplete reference genomes. Towards this, long-read sequencing technologies offer a solution to many of these challenges, and can be used to improve / assemble high quality reference genomes. In addition, long-read sequencing has been used in combination with short-read sequencing, to obtain high quality reference genomes for two parental species by sequencing an interspecific hybrid. This reduces the cost of generating such assemblies, as fewer sequence information is required than when these genomes are assembled independently.

As such, this project aims to assemble the genome of an F1 *E. urophylla* x *E. grandis* hybrid, to assemble high-quality reference genomes for both *E. urophylla* and *E. grandis*.

In order to obtain the best possible genome, multiple genome assembly programs will be tested as well as a trio-binning approach for haplotype assembly of *E. grandis* and *E. urophylla* as discussed in a paper by Koren *et al* 2018.

## **Directory descriptions**
<details><summary>Annotations</summary>
Directory containing all scripts related to structural and functional gene annotation. 

    grandis
        1_hisat  
        2_braker  
        3_gfacs  
        4_entap
    uro
        1_hisat  
        2_braker  
        3_gfacs  
        4_entap
</details>

<details><summary>Binned_assembly_benchmarks</summary>
Directory containing all scripts related to assembly benchmarking of binned reads by use of alternative genome assemblers.

    grandis
        canu  
        flye  
        necat  
        shasta
    uro
        canu  
        flye  
        necat  
        shasta
</details>

<details><summary>Circos plot visualisation</summary>
Directory containing scripts and required files for making Circos plots.
</details>

<details><summary>Contaminant check</summary>
Folder containing all scripts related to long (centrifuge) and short (kraken) read contaminant checks and removal. 
</details>

<details><summary>Final Thesis methods</summary>
Directory containing all methods/scripts for all analyses included in final MSc thesis.

    1 DNA Isolation 
    3 Read basecalling and QC 
    4 Read binning and contaminant removal
    5 Genome assembly and scaffolding   
    6 SV with SyRI 
    7 Repeatmasking
    8 Circos
    9 Supplementary notes
</details>

<details><summary>gene_synteny</summary>
Directory containing all scripts related to gene-based synteny analysis with MCScanX and MCScan-py.
</details>

<details><summary>Hybrid_assembly_Canu_binned</summary>
Directory containing scripts related to QC, assembly and scaffolding of Canu-binned long-reads.

    Contaminant_QC --> Centrifuge contaminant identification and removal
    ALLMAPS
        grandis
        urophylla
        polarstar_grandis
        polarstar_urophylla
        test
    masurca_gra --> scripts related to assembly of _E. grandis_ genome with MaSuRCA
    masurca_uro --> scripts related to assembly of _E. urophylla_ genome with MaSuRCA
    polar_star
        grandis --> scripts for downstream analyses of _E. grandis_ haplogenome before re-assembly of Chromosome 5/6 (reported in MSc thesis)    
        RE_analyses  
        SV_analysis --> scripts for downstream analyses of genome-wide haplogenome synteny before re-assembly of Chromosome 5/6 (reported in MSc thesis). This includes scripts for running SyRI and th ont-sv-pipeline.     
        urophylla --> scripts for downstream analyses of _E. urophylla_ haplogenome 
</details>

<details><summary>Illumina_read_trimming_and_QC</summary>
Directory containing scripts related to short-read illumina read QC and read trimming.
</details>

<details><summary>Long-read base-calling and QC</summary>
Directory containing scripts related to base-calling of Nanopore long-reads and adapter removal.
</details>

<details><summary>Long-read genome assembly</summary>
Directory containing scripts related to genome assembly based on only Nanopore long-reads without phasing.

    Canu genome assembly
    Falcon Genome assembly
    Flye genome assembly
    Shasta genome assembly
    wtdbg2 genome assembly
</details>

<details><summary>Masurca Genome Assembly</summary>
Directory containing scripts related to hybrid assembly of the F1 Nanopore long-reads and short-reads using the MaSuRCA genome assembler.
</details>

<details><summary>Masurca hybrid assembly purgehap</summary>
Folder containing scripts related to phasing of the FALCON based genome assembly using Purge-Haplotigs.
</details>

<details><summary>Masurca hybrid assembly repeat analysis</summary>
Scripts for repeatmasking of unphased MaSuRCA hybrid assembly.
</details>

<details><summary>Masurca parent genome assembly</summary>
Directory containing scripts for short-read only assembly of the parental short reads for the _E. grandis_ and _E. urophylla_ parental short-reads with MaSuRCA.
</details>

## **1) DNA Sequencing**

DNA was extracted from leaf tissue of and F1 *E. urophylla* x *E. grandis* hybrid. Sequencing of the hybrid
was performed using the ONT MinION and PromethION v9.4 platform.

Additionally, to use the trio-binning approach proposed by Koren *et al*, 151 bp PE Illumina sequencing data was generated for the F1 hybrid, as well as the *E. urophylla* and *E. grandis* parents.

The data generated for both platforms are displayed below, along with the theoretical estimated genome coverage:

**Illumina sequencing data:**

*	*E. urophylla* parent: 127.5 Gb, 196.2x
*	*E. grandis* parent: 141.6 Gb, 217.8x
*	F1 Hybrid: 116.1 Gb, 178.61x

**Nanopore sequencing data:**

*	MinION Run1: 11.18 Gb called, 9.5 Gb passed
*	MinION Run2: 2.55 Gb called, 2.32 Gb passed
*	PromethION: 61.59 Gb called, 56.69 Gb passed
*	Total: 75.32 Gb called, 68.51 Gb passed

Total estimated ONT coverage: 105.4x


- Raw long-read sequencing data located at:
`/archive/labs/wegrzyn/genomes/eucalyptus/2019_MSc/nanopore_raw`
- Base-called long-read sequencing data can be found at:
`/archive/labs/wegrzyn/genomes/eucalyptus/2019_MSc/nanopore_basecalled`
- Canu binned and contaminant removed reads can be found at:
`/archive/labs/wegrzyn/genomes/eucalyptus/2019_MSc/nanopore_canu_binned`
- Raw short-read sequencing data can be found at:
`/archive/labs/wegrzyn/genomes/eucalyptus/2019_MSc/illumina_raw_reads`
- Trimmed short-read sequencing data can be found at:
`/archive/labs/wegrzyn/genomes/eucalyptus/2019_MSc/illumina_trimmed_reads`

## **2) Long-read adapter removal and read QC**

Removal of sequencing adapters was performed using **PoreChop**. All scripts relating to the removal of read adapters from long read sequencing data can be found in the **Long-Read base-calling and QC** folder.

Scripts for base-calling with the **Guppy basecaller** (GPU version) are also available in this directory.

Illumina read QC (**FastQC**) and trimming (**Trimmomatic**) scripts can be found in the **Illumina read trimming and QC** folder.

Genome size was estimated for the F1 hybrid and both parents using GenomeScope and Jellyfish v2.

Results can be viewed for the:

*	[F1 hybrid](http://genomescope.org/analysis.php?code=y6aJStQvFNC3miEcfY1w)
*	[*E. urophylla* parent](http://genomescope.org/analysis.php?code=U0pwJgQ0cxIbaoZRU5XJ)
*	[*E. grandis* parent](http://genomescope.org/analysis.php?code=yJVSLG4asdhIYMWbWEKA)


## **3) Contaminant check**

This step is focused on removing reads that are contaminants (reads that are from organisms other than the species being sequenced). This step makes use of **Kraken2** (for Illumina reads) and **Centrifuge** (for long-reads).

Scripts for Kraken2 analysis and read removal, as well as Centrifuge analysis of raw ONT reads can be found in the [**Contaminant check**](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Contaminant%20check) directory.

**Input (Kraken2):**

*	PE Illumina reads short reads in FASTQ format
*	Kraken classification database
*	**kraken_Illumina.sh** script for generating unclassified and classified read sets

**Output:**

*	Classified and unclassified short read data for each read set (we will want to use the unclassified set going forward)

**Input (Centrifuge):**

*	Raw long reads in FASTQ format
*	Sequence index database of possible contaminants (human, archaea, bacterial and fungal)
*	**Centrifuge_euc.sh** script to generate a classification report

**Output:**

*	Centrifuge classification report

**Note:** Additional python scripts must be used to remove contaminated reads identified in the report. Scripts related to analysis and removal of contaminated reads from trio-binned ONT reads can be found in the [**Hybrid_assembly_Canu_binned/Contaminant_QC folder**](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Hybrid_assembly_Canu_binned/Contaminant_QC).

Resulting reports can be found here: [Centifuge (long-read) and Kraken2 (Illumina read) reports](https://docs.google.com/spreadsheets/d/1kgCXL3GB2o9xHkLu86ccIHauoIPRVLDWrz_ak5pV75w/edit?usp=sharing)


## **4) Genome assembly**

Multiple genome assembly programs were tested to obtain the best possible assembly for the F1 hybrid.


### Independant parental and hybrid short read genome assemblies

As a means to trio-binning, both parental genomes were independantly assembled using **Masurca** and 151 bp PE Illumina sequencing data. This also gives a baseline assembly with which to compare Hybrid and Long-read based assemblies.

Assembly scripts can be found in the [**Masurca parent genome assembly**](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Masurca%20parent%20genome%20assembly) directory as well as the [**Masurca Genome Assembly**](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Masurca%20Genome%20Assembly) folders.

[Short-read assembly statistics](https://drive.google.com/file/d/1iTPWglWvjcOwhq-c9e_g2sDNjAYNSnGb/view?usp=sharing)

**Input:**

*	Base-called Illumina sequencing reads in FASTQ format
*	**config.txt** file specifying input reads and parameters for the Masurca genome assembly run 
*	**masurca_ONT.sh** generates the assemble.sh file to run the Masurca assembly using your specified parameters and FASTQ files
 
**Output:**

*	**assemble.sh** script for running the assembly
*	Draft genome assembly named final.genome.scf.fasta in the CA directory


### SHASTA genome assembly

SHASTA genome assembler was used to assembly the F1 hybrid genome using uncorrected Nanopore sequencing reads (although I used the entire read set uncluding contaminants, we recommend using an uncontaminated read set).

All assembly scripts can be found in the [**Long-read genome assembly/Shasta genome assembly**](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Long-read%20genome%20assembly/Shasta%20genome%20assembly) folder.

Assembly quality assessment results can be seen [here](https://docs.google.com/spreadsheets/d/1aTbXIBGCENBpppWbEmMfj6gf7PoY4OCJ0c3e3TGMfqI/edit?usp=sharing)

**Input:**

*	Base-called PoreChopped Nanopore sequencing reads in FASTA format
*	**shasta.sh** script to run the shasta genoem assembly program with minimum input read length of 500 bp (this parameter may be changed)

**Output:**

*	Draft long-read genome assembly

### FALCON genome assembly

The FALCON genome assembler was used to assemble the F1 hybrid genome using Nanopore sequencing reads. The header within the FASTA file had to be changed for the FALCON program to allow assembly (this script can be found in the Falcon Genome assembly subdirectory.

FALCON also requires an environmental setup before running. All steps are better explained in the relevant directory.

Unfortunately FALCON-Unzip is not compatible with Nanopore data, so PurgeHaplotigs was used to remove redundant contigs and as potential alternative to haplotype phasing (see below).

All assembly scripts (including scripts related to the assembly quality assessment) can be found in the [**Long-read genome assembly/Falcon Genome assembly**](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Long-read%20genome%20assembly/Falcon%20Genome%20assembly) directory.

[Falcon genome assembly statistics results](https://drive.google.com/file/d/1iJyaJqX9VMLZh9BtXDC0F26bVpj3gOaV/view?usp=sharing)

**Input:**

*	Header modified Nanopore reads in FASTA format
*	**falcon_add.sh** script to activate environment for the python script to run in
*	**fc_run.py** script to specify run parameters (this must be optimised for your individual species)
*	**subreads.fasta.fofn** file specifying the path to the ONT FASTA reads (the modified ones)

**Output:**

*	Fasta assembly file (which likely contains redundant reads)

### Flye genome assembly

The Flye genome assembler was also tested to assemble th F1 hybrid genome using uncorrected (and unfiltered) Nanopore sequencing reads.

All assembly scripts related to the assembly (and assembly quality check) can be found in the [**Long-read genome assembly/Flye genome assembly**](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Long-read%20genome%20assembly/Flye%20genome%20assembly) folder.

Assembly quality results can be found [here](https://docs.google.com/spreadsheets/d/1aTbXIBGCENBpppWbEmMfj6gf7PoY4OCJ0c3e3TGMfqI/edit?usp=sharing)  

**Input:**

*	Raw ONT reads in FASTQ format
*	**flye_raw.sh** script to assemble the ONT reads

**Output:**
	
*	Assembled genome

### Masurca hybrid genome assembly

Masurca was used to assemble the F1 hybrid genome with both Nanopore and Illumina short reads.

All scripts related to the assembly (including assembly quality asseessment) can be found in the [**Masurca Genome Assembly**](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Masurca%20Genome%20Assembly) folder.

Results for the hybrid assembly (using short and long reads) for the F1 hybrid are attached below.

[Masurca hybrid genome assembly statistics](https://docs.google.com/spreadsheets/d/1Z1wfsx1yg7BcoNfln5DZ7vanzRXRWm0CB3Cz1lim50M/edit?usp=sharing)

**Input:**
	
*	Raw ONT long reads in FASTQ or FASTA format
*	PE Illumina (hybrid) short reads in FASTQ format
*	**config.txt** file specifying input reads and parameters for the Masurca genome assembly run (may need to be modified for your particular genome)
*	**masurca_ONT.sh** generates the assemble.sh file to run the Masurca assembly using your specified parameters and FASTQ files

**Output:**

*	**assemble.sh** script for running the assembly
*	Draft genome assembly named final.genome.scf.fasta in the CA directory

### Masurca assembly of Canu trio-binned reads

Phased genome assembly was tested using binned and uncontaminated ONT reads. Reads were binned with parental short-reads using Trio-Canu. The binned reads were assembled independantly using the Masurca hybrid genome assembler (using parental short reads and binned long-reads of the F1 hybrid. Two separate assemblies were run, one per bin). The scripts related to these assemblies can be found in the [**Hybrid_assembly_Canu_binned**](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Hybrid_assembly_Canu_binned) folder. All follwowing analysis based on the binned assemblies (including [genome size checks](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Hybrid_assembly_Canu_binned), [breakpoint inference](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Hybrid_assembly_Canu_binned), [scaffolding with genetic linkage maps](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Hybrid_assembly_Canu_binned) and [repeatmasking](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Hybrid_assembly_Canu_binned)) mentioned below are described in detail [here](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Hybrid_assembly_Canu_binned).

**Input:**

*	Uncontaminated and binned ONT long reads in FASTA format
*	Unclassified PE Illumina parental short reads in FASTQ format
*	**config.txt** file specifying input reads and parameters for the Masurca genome assembly run (may need to be modified for your particular genome)
*	**masurca_ONT.sh** generates the assemble.sh fiel to run the Masurca assembly using your specified parameters and FASTQ files

**Output:**

*	**assemble.sh** script for running the assembly
*	Draft genome assembly named final.genome.scf.fasta in the CA directory


## **5) Genome alignments to reference**

### Genome assembly comparisons (using minimap alignments via D-GENIES)

Mimimap2 alignments were performed for the following combinations to check if alignments do work as a possible alternative method for binning.

[D-GENIES](https://dgenies.toulouse.inra.fr/run) is an interactive website where these alignments can be performed using only the FASTA assembly file, and creates dotplots of the alignments (using either minimap2 or mashmap, which is chosen by the user). The resulting alignments can be downloaded and stored by the user to look at at a later time. The following comparisons were made and the alignment files were downloaded and are available [here](https://drive.google.com/drive/folders/1dCep6VYzy530tFMWPwCWXKY6KcCqZ7eK?usp=sharing).

*	Masurca hybrid assembly of 10Mb and longer contigs to *E. grandis* reference
*	Masurca hybrid long contigs to *E. grandis* reference
*	Masurca short read *E. urophylla* to reference *E. grandis*
*	Masurca short read *E. grandis* to reference *E. grandis*
*	Masurca short read *E. urophylla* to short read *E. grandis*
*	Masurca short read *E. urophylla* to Masurca hybrid aseembly
*	Masurca short read *E. grandis* to Masurca hybrid assembly
*	Masurca hybrid assembly to reference + chloroplast + mitochondrial
*	Masurca purged hybrid assembly to reference + chloroplast + mitochondrial
*	Falcon assembly to reference + chloroplast + mitochondrial

Alternatively one can use minimap and then using **nucmer** from the **Mummer** package one can create dotplot comparisons.


## **6) Genome assembly quality**

All genome assemblies were evaluated for quality using **BUSCO** to identify the number of core genes present in the assembled genome, and **QUAST** to check genome assembly statistics such as N50, # contigs etc.

All scripts related to these for each assembly can be found in their respective directories, but follow a simple layout:

*	`BUSCO: run_BUSCO.py -i /path/to/assembly.fasta -l /path/to/embryophyta/data -o /name_of_output_folder -m geno` 

*	`QUAST: quast.py /path/to/assembly.fasta -o Name_of_output_folder`

The spreadsheet containing the results is [here](https://docs.google.com/spreadsheets/d/1CExG0JOI0iO5IvEVQBJexyIM6ncKUYtlV5GaCt7qhXs/edit?usp=sharing).


## **7) Genome assembly improvements**

This section is focused on improving the draft genome to obtain reference quality genomes (not annotation).


### **1) Removal of redundant reads**

Redundant reads were removed from the the following assemblies using **Purge Haplotigs**

#### FALCON hybrid Genome assembly

A detailed guide to use of this tools can be found in the [Long-read genome assembly](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Long-read%20genome%20assembly) folder. Statistics of the assembly after removal of redundant reads using purge haplotigs are [here](https://docs.google.com/spreadsheets/d/1kgCXL3GB2o9xHkLu86ccIHauoIPRVLDWrz_ak5pV75w/edit?pli=1#gid=867340922).

Purge haplotigs histogram\
![alt text](Images/falconaligned2.sorted.bam.histogram_PurgeHap.png "Purge Haplotigs Histogram"){width=50%}


#### Masurca hybrid genome assembly

Statistics of the assembly after removal of redundant reads using purge haplotigs are also shown in the spreadsheet above.


### **2) Masking of repetitive elements**

Although I gave a numbered order, this step is separate from removal of redundant reads and can be performed in tandem.

This step is recommended before genome annotation or identification of structural variants but comes after scaffolding.

Repetitive elements were masked for the following genome assemblies:

*    Masurca hybrid genome assembly
*    *E. grandis* haplogenome
*    *E. urophylla* haplogenome

After repetitive elements have been masked one can obtain an estimate of the genome repetitiveness, as well as the type of elements involved in repetitiveness.

This will be included for the final genome you want to use and publish, as part of the genome annotation process.

The masking process is better explained in the [**Masurca hybrid assembly repeat analysis**](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Masurca%20hybrid%20assembly%20repeat%20analysis) directory.


### **3) Checking genome size**

To check if the assembled genome size is correct (and that the reduced assembly size compared to the estimated size is not due to reads not being assembled), **bwa mem** can be used along with the **flagstat** function from **samtools** to check alignment of raw Illumina reads to the assembled genome. If the assembly is correct and no elements are not being assembled, you would expect a mapping rate of **99%**. An example script can be found [here](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/blob/master/Hybrid_assembly_Canu_binned/masurca_gra/bwa_align.sh).


### **4) Inferring breakpoint for incorrectly assembled reads**

Inferring breakpoints for incorrectly assembled reads with ALLMAPS is not very accurate when using only a few thousand SNPS, therefore one may use [**PolarSTAR**](https://github.com/phasegenomics/polar_star) to infer breakpoints based on support (or lack of support) from long-read data. A detailed explanation can be found [here](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Hybrid_assembly_Canu_binned#5-inferring-breakpoints).


### **5) Genome scaffolding with genetic linkage maps**

This step should be performed before repeatmasking if planning on using the genome for annotation.

Genome scaffolding was performed using **ALLMAPS** using the following steps:

1) SNP probes were blasted aganst the assembled genome using **BLAST** to find the position of the SNPs used to construct high-density genetic linkage maps.

2) SNP probe positions were extracted and used in a **.csv** file as specified on the [**ALLMAPS**](https://github.com/tanghaibao/jcvi/wiki/ALLMAPS) webpage.

3) Run **ALLMAPS** script found in [**Hybrid_assembly_Canu_binned/ALLMAPS**](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Hybrid_assembly_Canu_binned/ALLMAPS) directory.

This is explained in more detail [here](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Hybrid_assembly_Canu_binned#6-genome-scaffolding).

## **8) Structural Variant (SV) identification and analysis**

This is described under the [Final Thesis Methods folder](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Final%20Thesis%20methods). Final process of RE masking is also described in this folder.

## **9) Genome Annotation**

This is described in the Annotation folder. All analysis are described in the Readme within the [Annotations](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/Annotations) folder. This process mainly consisted of the following pipeline using different input files and configurations

![Gene Annotation](https://mermaid.ink/img/pako:eNol0E1rwzAMBuC_InzKoKcdAxk4bbKNraMkuTU9GFtJDf7YHJkxSv_7lNQn23pAenUTOhoUpZhc_NVXlQg-uzEAH1kMyXqPBrovCT3-ZAzahhmMIvUEVfX23svhuapeoC6ks3NgWssjTNbhsoK6kx9Nt4l9cUporCY2M4ZHHeZW7hdY64dzax0hG-gpZU05KQevLLkzXVbchEGeNtyc2xw02RjYyBAiqfVxETvhMXllDQe6rSlGQVf0OIqSrwYnlR2NYgx3pipT7P-CFiU3xJ3I3xwMD1bNSXlRTsot_MtDU0zHx5K2Xd3_ARCpZDQ?type=png)


## **10) Gene synteny**

This is described in the [gene_synteny folder](https://gitlab.com/PlantGenomicsLab/eucalyptus-genome/-/tree/master/gene_synteny). Both MCScanX and MCScan_py were used.
