#!/bin/bash
#SBATCH --job-name=flye_gra
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 36
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=450G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load flye/2.4.2
flye --nano-raw /core/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/haplotype-URO_rmv_contam_reads.fasta \
 --genome-size 650m --out-dir ./flye_uro --threads 36
