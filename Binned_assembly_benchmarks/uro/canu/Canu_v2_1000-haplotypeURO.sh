#!/bin/sh
#SBATCH --job-name=canu_Uro
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=500G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o canuv2_%j.out
#SBATCH -e canuv2_%j.err

/isg/shared/apps/canu/2.1.1/bin/canu \
  -p Canu_v2_1000-haplotypeURO \
  -d Canu_v2_1000-haplotypeURO \
  -assemble \
  'genomeSize=650m' \
  '-minReadLength=1000' \
  '-useGrid=False' \
  -trimmed \
  -nanopore ./haplotype/haplotype-URO.fasta.gz \
> ./Canu_v2_1000-haplotypeURO.out 2>&1

exit 0

