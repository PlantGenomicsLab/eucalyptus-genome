#!/bin/bash
#SBATCH --job-name=necat_uro
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=ALL
#SBATCH --mem=450G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


module load NECAT/0.0.1

#necat.pl config uro_hap.txt
necat.pl correct uro_hap.txt
necat.pl assemble uro_hap.txt
necat.pl bridge uro_hap.txt


