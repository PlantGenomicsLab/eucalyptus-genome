#!/bin/bash
#SBATCH --job-name=purgehap_masurca
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=450G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load bedtools/2.25.0
module load samtools/1.3.1
module load minimap2/2.17
module load MUMmer/4.0.2
module load perl/5.28.1
module load R/3.5.1
module load purge_haplotigs/1.0

#. ~/miniconda3/etc/profile.d/conda.sh
#conda activate purge_haplotigs_env

#purge_haplotigs readhist -b ./../rawvsassembled.sorted.bam -g /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca2/CA.mr.41.15.15.0.02/final.genome.scf.fasta -t 16

#purge_haplotigs contigcov  -i ./rawvsassembled.sorted.bam.gencov  -l 20  -m 90 -h 165

purge_haplotigs purge -g /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca2/CA.mr.41.15.15.0.02/final.genome.scf.fasta -c coverage_stats.csv -b rawvsassembled.sorted.bam -t 16
