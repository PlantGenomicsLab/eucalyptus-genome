#!/bin/bash 
# Submission script for Xanadu 
#SBATCH --job-name=rpm 
#SBATCH -o repeatmasker-%j.output 
#SBATCH -e repeatmasker-%j.error 
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za 
#SBATCH --mail-type=END 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=16 
#SBATCH --partition=himem 
#SBATCH --qos=himem
#SBATCH --array=1-200%20 

# Run the program                 
echo Species.fa"$SLURM_ARRAY_TASK_ID".fa

module load RepeatMasker/4.0.6 
module load perl/5.24.0
export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5/
export PERLINC=/labs/Wegrzyn/perl5/lib/perl5/

# The pipe "|" here creates an output text file that can be checked for errors
RepeatMasker final.genome.scf.fasta"$SLURM_ARRAY_TASK_ID".fa -dir /scratch/alotter/repeatmodeler/RM_14789.TueFeb181554242020/repeatmasker_out -lib /scratch/alotter/repeatmodeler/RM_14789.TueFeb181554242020/consensi.fa -pa 4 -gff -a -noisy -low -xsmall| tee output_spp."$SLURM_ARRAY_TASK_ID".txt

