#!/bin/bash
#SBATCH --job-name=uro_masurca
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=500G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/masurca_parents/urophylla/%x_%j.out
#SBATCH -e /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/masurca_parents/urophylla/%x_%j.err

module load singularity/3.1.1
module load MaSuRCA/3.3.4


masurca config.txt

./assemble.sh

