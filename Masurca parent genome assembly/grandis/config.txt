DATA
PE= aa 350 50 /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_1.fastq.gz /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_2.fastq.gz 
#NANOPORE=/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/GUF1_PoreChopped_MinIONandPromethION.fastq
END

PARAMETERS
EXTEND_JUMP_READS=0
GRAPH_KMER_SIZE = auto
USE_LINKING_MATES = 0
LIMIT_JUMP_COVERAGE = 300
CA_PARAMETERS =  cgwErrorRate=0.15
KMER_COUNT_THRESHOLD = 1
NUM_THREADS = 28
JF_SIZE = 6500000000
SOAP_ASSEMBLY=0
FLYE_ASSEMBLY=0
END

