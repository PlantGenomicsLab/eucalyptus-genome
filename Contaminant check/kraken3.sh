#!/bin/bash
#SBATCH --job-name=kraken2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --mem=200G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o kraken2_%j.out
#SBATCH -e kraken2_%j.err

module load kraken/2.0.8-beta
#kraken/1.0
module load jellyfish/2.2.6
kraken2 --db /isg/shared/databases/kraken2/Standard/ --report FK118_kraken2_report2.out --fastq-input --paired --threads 8 /projects/EBP/CBC/eucalyptus/rawReads/Illumina/F1_FK118_1.fastq.gz /projects/EBP/CBC/eucalyptus/rawReads/Illumina/F1_FK118_2.fastq.gz > Euc_F1FK118_kraken2.out
kraken2 --db /isg/shared/databases/kraken2/Standard/ --report FK1756_kraken2_report2.out --fastq-input --paired --threads 8 /projects/EBP/CBC/eucalyptus/rawReads/Illumina/URO_FK1556_1.fastq.gz /projects/EBP/CBC/eucalyptus/rawReads/Illumina/URO_FK1556_2.fastq.gz > Euc_FK1756_kraken2.out
kraken2 --db /isg/shared/databases/kraken2/Standard/ --report FK1758_kraken2_report2.out --fastq-input --paired --threads 8 /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_1.fastq.gz /projects/EBP/CBC/eucalyptus/rawReads/Illumina/GRA_FK1758_2.fastq.gz > Euc_FK1758_kraken2.out
kraken2 --db /isg/shared/databases/kraken2/Standard/ --report FK1755_kraken2_report2.out --fastq-input --paired --threads 8 /projects/EBP/CBC/eucalyptus/rawReads/Illumina/URO_FK1755_1.fastq.gz /projects/EBP/CBC/eucalyptus/rawReads/Illumina/URO_FK1755_2.fastq.gz  > Euc_FK1755_kraken2.out
