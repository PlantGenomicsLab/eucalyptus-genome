# Genome assembly with the MaSuRCa genome assembler

## **F1 Hybrid genome assembly**

The F1 hybrid genome was assembled using the raw ONT PoreChopped long-reads and raw Illumina short-reads.

**Input:**

*	Raw Illumina short-reads in FASTQ format
*	Raw ONT long-reads in FASTQ format
*	**masurca_ONT.sh** script to specify assembly resources usage
*	**config.txt** file to specify genome assembly parameters

**Output:**

*	**assemble.sh** script specifying genome assembly
*	Draft hybrid genome assembly (**final.genome.scf.fasta**)

**Note:** Masurca tends to perform poorly i.t.o. purging redundant reads from the genome. As such I would recommend using **PurgeHaplotigs** or **PurgeDup** to remove redundant reads (as in the **Masurca hybrid assembly repeat analysis directory**).


As a baseline, the genomes of *E. urophylla*, *E. grandis* and the F1 hybrid was also assembled with the Masurca assembler using only Illumina short reads. All scripts for both parents are in the relevant directories (**Masurca parent genome assembly directory**) and the scripts for the F1 hybrid assembly can be found in the **Masurce short read assembly** directly.

**Input:**

*	Raw Illumina short-reads in FASTQ format
*	**masurca_ONT.sh** script to specify assembly resources usage
*	**config.txt** file to specify genome assembly parameters

**Output:**

*	**assemble.sh** script specifying genome assembly
*	Draft hybrid genome assembly (**final.genome.scf.fasta**)

All scripts related to assembly quality assessment can be found in relevant directories.


