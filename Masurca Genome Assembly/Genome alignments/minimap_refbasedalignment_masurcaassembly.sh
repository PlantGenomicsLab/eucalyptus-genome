#!/bin/bash
#SBATCH --job-name=minimap2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/MM/%x_%j.out
#SBATCH -e /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/MM/%x_%j.err

module load minimap2/2.17
#minimap2 -ax map-ont /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Egrandis_genome_v2/Egrandis_297_v2.0.fa /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/GUF1_PoreChopped_MinIONandPromethION.fastq > /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/ont_aligned_reads.sam

#minimap2 -ax map-ont /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Egrandis_genome_v2/Egrandis_297_v2.0.fa /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca2/CA.mr.41.15.15.0.02/final.genome.scf.fasta > /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca2/ont_masurcaaligned_reads.sam

#minimap2 -ax map-ont /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Egrandis_genome_v2/Egrandis_297_v2.0.fa /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca2/CA.mr.41.15.15.0.02/final.genome.scf.10Mb.fasta > /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca2/ont_masurcaaligned_reads10Mb.sam

minimap2 -ax asm5 /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Egrandis_genome_v2/Egrandis_297_v2.0.fa /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca2/CA.mr.41.15.15.0.02/final.genome.scf.10Mb.fasta > /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/MM/ont_masurcaaligned_reads10Mb_asm.sam

minimap2 -ax asm5 /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Egrandis_genome_v2/Egrandis_297_v2.0.fa /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca2/CA.mr.41.15.15.0.02/final.genome.scf.fasta > /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/MM/ont_masurcaaligned_reads_asm.sam


module load samtools/1.9
samtools view -S -b ont_masurcaaligned_reads_asm.sam > ont_masurcaaligned_reads_asm.bam
samtools sort ont_masurcaaligned_reads_asm.bam -o ont_masurcaaligned_reads_asm.sorted.bam
samtools index ont_masurcaaligned_reads_asm.sorted.bam
samtools depth ont_masurcaaligned_reads_asm.sorted.bam > depth_asm.txt
samtools depth ont_masurcaaligned_reads.sorted.bam | awk '{sum+=$3} END { print "Average = ",sum/NR}' > total_asm_avg.txt
samtools view -S -b ont_masurcaaligned_reads10Mb_asm.sam > ont_masurcaaligned_reads10Mb_asm.bam
samtools sort ont_masurcaaligned_reads10Mb_asm.bam -o ont_masurcaaligned_reads10Mb_asm.sorted.bam
samtools index ont_masurcaaligned_reads10Mb_asm.sorted.bam
samtools depth ont_masurcaaligned_reads10Mb_asm.sorted.bam > depth_10Mb_asm.txt
samtools depth ont_masurcaaligned_reads10Mb_asm.sorted.bam | awk '{sum+=$3} END { print "Average = ",sum/NR}' > 10Mb_asm_avg.txt

