#!/bin/bash
#SBATCH --job-name=quast_long
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=30G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca_short/%x_%A.out
#SBATCH -e /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca_short/%x_%A.err

module load quast/5.0.2
quast.py /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca_short/CA/final.genome.scf.fasta -o Masurca1_quast_short

#quast.py /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca_short/CA/genome_scf_3kb.fasta -o Masurca1_quast_short_3kb
