#!/bin/bash
#SBATCH --job-name=busco
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=75G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

#module load busco/4.0.2
module load busco/3.0.2b
module unload augustus
export PATH=/home/FCAM/alotter/augustus-3.2.3/3.2.3/bin:/home/FCAM/alotter/augustus-3.2.3/3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/3.2.3/config
###if your run crashes uncomment the following:
#module unload blast/2.7.1
#module load blast/2.2.29 

#run_BUSCO.py -i /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca2/CA.mr.41.15.15.0.02/final.genome.scf.fasta -l /isg/shared/databases/busco_lineages/embryophyta_odb10/ -o Busco_EucGUF1_Masurca2_wholeGenome -m geno
#busco -i /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca2/CA.mr.41.15.15.0.02/final.genome.scf.fasta -l /isg/shared/databases/busco_lineages/embryophyta_odb10/ -o Busco402_EucGUF1_Masurca2 -m geno -c 8
run_BUSCO.py -i /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/final.genome.scf.fasta -l /isg/shared/databases/busco_lineages/embryophyta_odb10/ -o Busco_uro_hybrid2 -m geno 

