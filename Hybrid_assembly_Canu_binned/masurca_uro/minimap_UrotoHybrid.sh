#!/bin/bash
#SBATCH --job-name=UroBintoHybrid
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.17
minimap2 -ax asm20 /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Masurca2/CA.mr.41.15.15.0.02/final.genome.scf.fasta /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/final.genome.scf.fasta > /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/alignments/UROBintoHybrid.sam

module load samtools/1.9
samtools view -S -b UROBintoHybrid.sam > UROBintoHybrid.bam
samtools sort UROBintoHybrid.bam > UROBintoHybrid.sorted.bam
samtools index UROBintoHybrid.sorted.bam
samtools depth UROBintoHybrid.sorted.bam > UroBintoHybrid.txt
samtools depth UROBintoHybrid.sorted.bam | awk '{sum+=$3} END { print "Average = ",sum/NR}' > total_UroBintoHybrid.txt
