#!/bin/bash
#SBATCH --job-name=Allmaps_Uro
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=30
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load ALLMAPS/144a067

# Prepare ALLMAPS input
#python -m jcvi.assembly.allmaps merge UroAllMap1.final.csv -o UroAllMap1.bed -w weights.txt

# Run ALLMAPS
#python -m jcvi.assembly.allmaps path UroAllMap1.bed /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/final.genome.scf.fasta -w weights.txt

# Estimate gap lengths
python -m jcvi.assembly.allmaps estimategaps UroAllMap1.bed
