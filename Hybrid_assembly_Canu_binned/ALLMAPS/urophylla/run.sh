#!/bin/bash
#SBATCH --job-name=Allmaps_Uro
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=150G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load ALLMAPS/144a067

# Prepare ALLMAPS input
#python -m jcvi.assembly.allmaps merge UroAllMap1.final.csv Grandis_allmaps.final.csv -o Maps2.bed -w weights.txt

# Run ALLMAPS
#python -m jcvi.assembly.allmaps path Maps2.bed /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/final.genome.scf.fasta -w weights.txt

# Estimate gap lengths
python -m jcvi.assembly.allmaps estimategaps Maps2.bed

mv Maps2.estimategaps.agp Maps2.chr.agp 
python -m jcvi.assembly.allmaps build Maps2.bed /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/final.genome.scf.fasta

#python -m jcvi.assembly.allmaps path Maps2.bed /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/final.genome.scf.fasta -w weights.txt
