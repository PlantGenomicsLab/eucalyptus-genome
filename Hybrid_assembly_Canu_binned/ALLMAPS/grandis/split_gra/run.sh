#!/bin/bash
#SBATCH --job-name=Allmaps_Gra
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=150G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load ALLMAPS/144a067

# Prepare ALLMAPS input
#python -m jcvi.assembly.allmaps merge UroAllMap1.final.csv Grandis_allmaps.final.csv -o Maps2.bed -w weights.txt

# Run ALLMAPS
#python -m jcvi.assembly.allmaps path Maps2.bed /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/final.genome.scf.fasta -w weights.txt

# Estimate gap lengths
#python -m jcvi.assembly.allmaps estimategaps Maps2.bed

#mv Maps2.estimategaps.agp Maps2.chr.agp 
#python -m jcvi.assembly.allmaps build Maps2.bed /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/final.genome.scf.fasta

#python -m jcvi.assembly.allmaps path Maps2.bed /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/final.genome.scf.fasta -w weights.txt

#Split scaffold where it matches 4 or maroe marker to another LG
#python -m jcvi.assembly.allmaps split GU2.bed --chunk=4 > breakpoints.bed

#Find gaps in assembly
#python -m jcvi.formats.fasta gaps genome.fasta

#module load BEDtools/2.29.0
#intersectBed -wao -a breakpoints.bed -b genome.gaps.bed >breakpoints.genome.bed


#Refine breakpoint
#python -m jcvi.assembly.patch refine breakpoints.bed genome.gaps.bed

#python -m jcvi.formats.sizes agp genome.fasta
#python -m jcvi.formats.agp mask genome.fasta.agp breakpoints.genome.refined.bed --splitobject --splitsingle
#python -m jcvi.formats.agp build genome.fasta.masked.agp genome.fasta genome.SPLIT.fasta

#python -m jcvi.assembly.allmaps merge Uro_allmap.csv Gra_allmap.csv -o Split_maps.bed -w weights.txt

python -m jcvi.assembly.allmaps path Split_maps.bed genome.SPLIT.fasta -w weights.txt
