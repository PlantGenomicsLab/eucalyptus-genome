#!/bin/bash
#SBATCH --job-name=masurca
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=500G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o canuv2_%j.out
#SBATCH -e canuv2_%j.err

# canu fucntion
module load canu/2.1.1

#split reads into haplotypes
canu -haplotype\
	-p Canu_v2_1000 -d /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Canu_assembly/canuv2/ genomeSize=650m \
	-minReadLength=1000 \
        -useGrid=False \
	-haplotypeURO /projects/EBP/CBC/eucalyptus/rawReads/Illumina/raw_files/URO_FK1556_* \
	-haplotypeGRA /projects/EBP/CBC/eucalyptus/rawReads/Illumina/raw_files/GRA_FK1758_* \
        -trimmed -nanopore /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/GUF1_PoreChopped_MinIONandPromethION.fastq

