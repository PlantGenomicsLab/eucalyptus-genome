#!/bin/bash
#SBATCH --job-name=hisat2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 19
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=300G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load hisat2/2.1.0
# building index
hisat2-build -s -p 19 urophylla.fasta.masked urophylla_index


# aligning with tunning for splice precision
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 GU3yo/trim_M546_1.fastq -2 GU3yo/trim_M546_1.fastq -S MS10.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 GU3yo/trim_M640-1.fq GU3yo/trim_M640-2.fq -S MS11.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 GU3yo/trim_M645-1.fq GU3yo/trim_M645--2.fq -S MS12.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 GU3yo/trim_M648-1.fq GU3yo/trim_M648-2.fq -S MS13.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 GU3yo/trim_M658-1.fq GU3yo/trim_M658-2.fq -S MS14.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 GU3yo/trim_M661-1.fq GU3yo/trim_M661-2.fq -S MS15.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 GU3yo/trim_M668-1.fq GU3yo/trim_M668-2.fq -S MS16.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 GU3yo/trim_M715-1.fq GU3yo/trim_M715-2.fq -S MS17.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 GU3yo/trim_M723-1.fq GU3yo/trim_M723-2.fq -S MS18.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 GU3yo/trim_M728-1.fq GU3yo/trim_M728-2.fq -S MS19.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 GU3yo/trim_M809-1.fq GU3yo/trim_M809-2.fq -S MS20.sam
hisat2 --threads 19 --pen-noncansplice 18 --max-intronlen 15000 --time --reorder -q -x urophylla_index -1 GU3yo/trim_M826-1.fq GU3yo/trim_M826-2.fq MS21.sam


module load samtools/1.9
# Convert HISAT2 sam files to bam files and sort by aligned position
samtools sort -@ 40 MS10.sam | samtools view -bS -F 0x04 - > MS10.OnlyAligned.bam
samtools sort -@ 40 MS11.sam | samtools view -bS -F 0x04 - > MS11.OnlyAligned.bam
samtools sort -@ 40 MS12.sam | samtools view -bS -F 0x04 - > MS12.OnlyAligned.bam
samtools sort -@ 40 MS13.sam | samtools view -bS -F 0x04 - > MS13.OnlyAligned.bam
samtools sort -@ 40 MS14.sam | samtools view -bS -F 0x04 - > MS14.OnlyAligned.bam
samtools sort -@ 40 MS15.sam | samtools view -bS -F 0x04 - > MS15.OnlyAligned.bam
samtools sort -@ 40 MS16.sam | samtools view -bS -F 0x04 - > MS16.OnlyAligned.bam
samtools sort -@ 40 MS17.sam | samtools view -bS -F 0x04 - > MS17.OnlyAligned.bam
samtools sort -@ 40 MS18.sam | samtools view -bS -F 0x04 - > MS18.OnlyAligned.bam
samtools sort -@ 40 MS19.sam | samtools view -bS -F 0x04 - > MS18.OnlyAligned.bam
samtools sort -@ 40 MS20.sam | samtools view -bS -F 0x04 - > MS20.OnlyAligned.bam
samtools sort -@ 40 MS21.sam | samtools view -bS -F 0x04 - > MS21.OnlyAligned.bam

#merge bamfiles
samtools merge -nurlf -b bamlist.fofn all.bam
