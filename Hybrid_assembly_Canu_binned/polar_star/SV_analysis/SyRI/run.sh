#!/bin/bash
#SBATCH --job-name=syri
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=400G
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%A.out
#SBATCH -e %x_%A.err

. ~/miniconda3/etc/profile.d/conda.sh
#conda activate ~/miniconda3/envs/py35_new/
conda activate ~/miniconda3/envs/py35/
cwd="."     # Change to working directory
PATH_TO_SYRI="./syri/bin/syri" #Change the path to point to syri executable
PATH_TO_PLOTSR="./syri/bin/plotsr" #Change the path to point to plotsr executable

#mkdir urohapvsv2.0
mkdir urovsgrahap


# Using minimap2 for generating alignment. Any other whole genome alignment tool can also be used.
#minimap2 -ax asm5 --eqx ../../nglmr/grandis.masked.fasta ../../nglmr/uro.masked.fasta > out.sam
#minimap2 -ax asm5 --eqx sorted_chr.fa ./grandis.masked.fasta > out.sam
#minimap2 -ax asm5 --eqx sorted_chr.fa ./uro.masked.fasta > out.sam
minimap2 -ax asm5 --eqx uro.masked.fast grandis.masked.fasta > out.sam

samtools view -b out.sam > out.bam

#python3 $PATH_TO_SYRI -c out.bam -r Egrandis_297_v2.0.fa ../../nglmr/grandis.masked.fasta -d out.filtered.delta -k -F B --no-chrmatch
#python3 $PATH_TO_SYRI -c out.bam -r sorted_chr.fa -q ./grandis.masked.fasta -k -F B --no-chrmatch 
#python3 $PATH_TO_SYRI -c out.bam -r sorted_chr.fa -q ./uro.masked.fasta -k -F B --no-chrmatch
python3 $PATH_TO_SYRI -c out.sam -r ./uro.masked.fasta -q ./grandis.masked.fasta -k -F B --no-chrmatch

# Using SyRI to identify genomic rearrangements from whole-genome alignments generated using MUMmer. A .tsv (out.filtered.coords) file is used as the input.
# Whole genome alignment. Any other alignment can also be used.
#nucmer --maxmatch -c 100 -b 500 -l 50 sorted_chr.fa ./grandis.masked.fasta 
#nucmer --maxmatch -c 100 -b 500 -l 50 sorted_chr.fa ./uro.masked.fasta
nucmer --maxmatch -c 100 -b 500 -l 50 ./uro.masked.fasta ./grandis.masked.fasta

# Remove small and lower quality alignments
delta-filter -m -i 90 -l 100 out.delta > out.filtered.delta
# Convert alignment information to a .TSV format as required by SyRI
show-coords -THrd out.filtered.delta > out.filtered.coords

#python3 $PATH_TO_SYRI -c out.filtered.coords -d out.filtered.delta -r sorted_chr.fa -q ./uro.masked.fasta -k --no-chrmatch
python3 $PATH_TO_SYRI -c out.filtered.coords -d out.filtered.delta -r ./uro.masked.fasta -q ./grandis.masked.fasta -k --no-chrmatch


# Plot genomic structure
python3 $PATH_TO_PLOTSR syri.out ./uro.masked.fasta ./grandis.masked.fasta -o pdf -d 600

show-coords -THrd out.delta > out.coords

#nucmer --maxmatch  -l 100 -c 500 sorted_chr.fa ./grandis.masked.fasta -prefix assemblytics
#nucmer --maxmatch  -l 100 -c 500 sorted_chr.fa ./uro.masked.fasta -prefix assemblytics
nucmer --maxmatch -l 100 -c 500 ./uro.masked.fasta ./grandis.masked.fasta -prefix assemblytics

mv -v *.out /urovsgrahap
