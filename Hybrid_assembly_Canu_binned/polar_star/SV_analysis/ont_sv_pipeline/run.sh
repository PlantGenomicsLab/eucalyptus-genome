#!/bin/bash
#SBATCH --job-name=SV
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=300G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o SV_%j.out
#SBATCH -e SV_%j.err

. ~/miniconda3/etc/profile.d/conda.sh
conda activate pipeline-structural-variation
#conda activate ont_tutorial_sv

#snakemake -j 20 all --config input_fastq=../../../../../../haplotype-URO.fasta reference_fasta=../../nglmr/grandis.masked.fasta
#snakemake -j 30 eval --config input_fastq=../../../../../../haplotype-URO.fasta reference_fasta=../../nglmr/grandis.masked.fasta min_sv_length=50

#R --slave -e 'rmarkdown::render("ont_tutorial_sv.Rmd", "html_document")'

#snakemake -j 20 all --config input_fastq=../../../../../../haplotype-GRA.fasta reference_fasta=../../nglmr/uro.masked.fasta
#snakemake -j 30 eval --config input_fastq=../../../../../../haplotype-GRA.fasta reference_fasta=../../nglmr/uro.masked.fasta min_sv_length=50


#snakemake -j 20 all --config input_fastq=../../../../../../haplotype-GRA.fasta reference_fasta=./Egrandis_297_v2.0.fa 
#snakemake -j 20 eval --config input_fastq=../../../../../../haplotype-GRA.fasta reference_fasta=./Egrandis_297_v2.0.fa min_sv_length=50

snakemake -j 20 all --config input_fastq=../../../../../../haplotype-URO.fasta reference_fasta=./Egrandis_297_v2.0.fa
snakemake -j 20 eval --config input_fastq=../../../../../../haplotype-URO.fasta reference_fasta=./Egrandis_297_v2.0.fa min_sv_length=50
