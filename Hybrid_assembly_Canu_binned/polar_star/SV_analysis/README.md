# Structural Varaiant Identification and analysis

This section is focused on programs used for identification and analysis of SV based on sequence data (a later section will focus on gene based synteny). I make use of two programs that are recommended and are compatible with LRS data:


## 1) [SyRI (Synteny and Rearrangement Identifier)](https://schneebergerlab.github.io/syri/)


## 2) [Pipeline SV (ONT)](https://github.com/nanoporetech/pipeline-structural-variation) 


