#!/bin/bash
#SBATCH --job-name=UroBintoRef
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load minimap2/2.17
minimap2 -ax asm20 /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/Egrandis_genome_v2/AllChr_mt_ch.fasta /projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_gra2/CA.mr.41.15.15.0.02/final.genome.scf.fasta > GraBintoGraref.sam

module load samtools/1.9
samtools view -S -b GraBintoGraref.sam > GraBintoGraref.bam
samtools sort GraBintoGraref.bam -o GraBintoGraref.sorted.bam
samtools index GraBintoGraref.sorted.bam
samtools depth GraBintoGraref.sorted.bam > GraBintoGraref.txt
samtools depth GraBintoGraref.sorted.bam | awk '{sum+=$3} END { print "Average = ",sum/NR}' > total_GraBintoGraref.txt


module load deeptools/2.0
bamCoverage -b GraBintoGraref.sorted.bam -of bedgraph -bs 1000000 -o GraBintoGraref.sorted.bed -p 8

