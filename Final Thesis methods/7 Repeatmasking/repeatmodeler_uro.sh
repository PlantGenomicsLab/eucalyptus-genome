#!/bin/bash
#SBATCH --job-name=rpmodel
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=250G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o repeatmodeler-%j.output
#SBATCH -e repeatmodeler-%j.error

# Run the program
export DATADIR='/projects/EBP/CBC/eucalyptus/rawReads/Nanopore/canu_binned_reads/masurca_uro/CA.mr.41.15.15.0.02/polar_star/uro_allmaps/'
export SEQFILE="GU2.fasta"
export DATABASE="urophylla"


WORKDIR=/scratch/$USER/repeatmodeler/urophylla/$JOB_ID 
mkdir -p $WORKDIR 
cp $DATADIR/$SEQFILE $WORKDIR 
cd $WORKDIR

# Note that modules listed here may not be up to date or present when you are trying to run this script
module load RepeatModeler/1.0.8 
module load rmblastn/2.2.28 
module load perl/5.24.0 
export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5/ 
export PERLINC=/labs/Wegrzyn/perl5/lib/perl5/

#module load perl/5.28.1

BuildDatabase -name "urophylla" /scratch/$USER/repeatmodeler/urophylla/GU2.fasta 
RepeatModeler -engine ncbi -pa 22 -database $DATABASE
