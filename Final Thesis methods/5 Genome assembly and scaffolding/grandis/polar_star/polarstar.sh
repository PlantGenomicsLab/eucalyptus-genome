#!/bin/bash
#SBATCH --job-name=polarstar
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 20
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=300G
#SBATCH --mail-user=anneri.lotter@fabi.up.ac.za
#SBATCH -o polarstar_%j.out
#SBATCH -e polarstar_%j.err

. ~/miniconda3/etc/profile.d/conda.sh
conda activate snakemake
snakemake --cores 16 -p -s Snakefile 
