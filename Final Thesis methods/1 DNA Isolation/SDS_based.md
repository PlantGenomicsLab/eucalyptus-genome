### SDS-based DNA isolation

- Heat up lysis buffer for 20 min at 55°C
- Grind of 2.5g of plant material to a fine powder with liquid nitrogen and a pestle and mortar (5g Plant material can be used)
- Add 20 ml Lysis buffer.
- Incubate for 15 min at 55°C
- Centrifuge at 3000 rcf for 30 min at room temperature. (Centrifuge at 8000 rcf for 12 min at room temperature).
- Pipette of the supernatant gently into a new tube and add 0.5V of chloroform
- Shake gently by inverting tube
- Centrifuge at 3000 rcf for 30 min at room temperature. (Centrifuge at 8000 rcf for 12 min at room temperature)
- Pipette of the supernatant gently into a new tube and add 1 V of 24:1 chloroform: Iso-amyl alcohol
- Centrifuge at 3000 rcf for 30 min at room temperature. (Centrifuge at 8000 rcf for 12 min at room temperature)
- Pipette of the supernatant gently into a new tube and add 0.3 V of 2M NaCl and 2.5V of 100% ice cold Ethanol.
- Incubate for 30 min on ice (overnight) 
- Centrifuge at 3000 rcf for 30 min at room temperature. (Centrifuge at 8000 rcf for 12 min at room temperature)
- Discard supernatant taking care not to dislodge the pellet.
- Wash twice with ice cold 70% ethanol by adding the ethanol and centrifuging for 1 min at 12000g and removing supernatant
- Allow pellet to airdry and resuspend in TE containing RNAse
- Incubate for 1 hour at 37 -50°C
- Pipette of the supernatant gently into a new tube and add 1 V of 24:1 chloroform: Iso-amyl alcohol
- Centrifuge at 3000 rcf for 30 min at room temperature. (Centrifuge at 8000 rcf for 12 min at room temperature)
- Pipette of the supernatant gently into a new tube and add 0.1 V of 7.5 NH4Ac and 2.5V of 100% ice cold Ethanol.
- Incubate for 30 min on ice
- Centrifuge at 3000 rcf for 30 min at room temperature. (Centrifuge at 8000 rcf for 12 min at room temperature)
- Discard supernatant taking care not to dislodge the pellet.
- Wash three times with ice cold 70% ethanol by adding the ethanol and centrifuging for 1 min at 12000g and removing supernatant
- Allow pellet to airdry and resuspend in ddH20

| Lysis buffer | (40ml) | (10ml) |
| ------ | ------ | ------ |
| 1% SDS (20% stock) | 2 ml | 0.5 ml |
| 100 mM Tris-HCl pH 8.0 (1 M stock) | 4 ml | 1 ml |
| 1.4 M NaCl (5 M stock) | 11.2 ml | 2.8 ml |
| 20 mM EDTA pH 8.0 (0.5 M) | 1.6 ml | 0.4 ml |
| 1:25(0.04%) PVP 40 (10% stock) | 8 ml | 2 ml |
| Beta-Mercaptoethanol | 0.2 ml | 0.05 ml |
| 20 ug/ml Proteinase K (20mg/ml stock) | 0.04 ml | 0.01 ml |
| ddH2O | 13 ml | 3.25 ml |


| TE | 10 ml |
| ------ | ------ |
| 10mM Tris-HCl pH 8.0 (1M stock) | 0.1 ml |
| 1mM EDTA pH 8.0 (0.5M) | 0.02 ml |
| ddH2O | 9.88 ml |
| 1:70 V RNAse(10mg/ml) | 5 ul/350ul TE |

