#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=72:00:00
#PBS -q bigmem
#PBS -o /nlustre/users/anneri/genome_size/WGS_data/E_dunnii/jellyfish.o
#PBS -e /nlustre/users/anneri/genome_size/WGS_data/E_dunnii/jellyfish.e
#PBS -k oe
#PBS -m ae
#PBS -M anneri.lotter@fabi.up.ac.za


module load jellyfish-2.3.0
cd /nlustre/users/anneri/genome_size/WGS_data/E_dunnii/
#120830_I128_FCD1CUKACXX_L1_SZAXPI013555-15_1.fq 120830_I128_FCD1CUKACXX_L1_SZAXPI013555-15_2.fq

#120830_I128_FCD1CUKACXX_L1_SZAXPI013556-84_1.fq 120830_I128_FCD1CUKACXX_L1_SZAXPI013556-84_2.fq

#120830_I128_FCD1CUKACXX_L2_SZAXPI013557-19_1.fq 120830_I128_FCD1CUKACXX_L2_SZAXPI013557-19_2.fq

#120830_I128_FCD1CUKACXX_L2_SZAXPI013558-35_1.fq 120830_I128_FCD1CUKACXX_L2_SZAXPI013558-35_2.fq

#120830_I128_FCD1CUKACXX_L3_SZAXPI013559-20_1.fq 120830_I128_FCD1CUKACXX_L3_SZAXPI013559-20_2.fq

#120830_I128_FCD1CUKACXX_L3_SZAXPI013560-94_1.fq 120830_I128_FCD1CUKACXX_L3_SZAXPI013560-94_2.fq

jellyfish count -t 28 -C -m 21 -s 100G -o BV170_21mer_out --min-qual-char=? 121019_I244_FCC19J7ACXX_L5_SZAIPI015472-113_1.fq 121019_I244_FCC19J7ACXX_L5_SZAIPI015472-113_2.fq
jellyfish histo -o BV170_21mer_out.histo BV170_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BV175_21mer_out --min-qual-char=? 121019_I244_FCC19J7ACXX_L5_SZAIPI015474-136_1.fq 121019_I244_FCC19J7ACXX_L5_SZAIPI015474-136_2.fq
jellyfish histo -o BV175_21mer_out.histo BV175_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BV100_21mer_out --min-qual-char=? 121019_I244_FCD1E2TACXX_L5_SZAIPI015465-30_1.fq 121019_I244_FCD1E2TACXX_L5_SZAIPI015465-30_2.fq
jellyfish histo -o BV100_21mer_out.histo BV100_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BV157_21mer_out --min-qual-char=? 121019_I244_FCD1E2TACXX_L5_SZAIPI015470-62_1.fq 121019_I244_FCD1E2TACXX_L5_SZAIPI015470-62_2.fq
jellyfish histo -o BV157_21mer_out.histo BV157_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BV164_21mer_out --min-qual-char=? 121019_I244_FCD1E2TACXX_L6_SZAIPI015471-109_1.fq 121019_I244_FCD1E2TACXX_L6_SZAIPI015471-109_2.fq
jellyfish histo -o BV164_21mer_out.histo BV164_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BV174_21mer_out --min-qual-char=? 121019_I244_FCD1E2TACXX_L6_SZAIPI015473-133_1.fq 121019_I244_FCD1E2TACXX_L6_SZAIPI015473-133_2.fq
jellyfish histo -o BV174_21mer_out.histo BV174_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BV138_21mer_out --min-qual-char=? 121019_I248_FCC19K0ACXX_L5_SZAIPI015466-31_1.fq 121019_I248_FCC19K0ACXX_L5_SZAIPI015466-31_2.fq
jellyfish histo -o BV138_21mer_out.histo BV138_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BV139_21mer_out --min-qual-char=? 121019_I248_FCC19K0ACXX_L5_SZAIPI015467-33_1.fq 121019_I248_FCC19K0ACXX_L5_SZAIPI015467-33_2.fq
jellyfish histo -o BV139_21mer_out.histo BV139_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BV143_21mer_out --min-qual-char=? 121019_I248_FCC19K0ACXX_L6_SZAIPI015468-35_1.fq 121019_I248_FCC19K0ACXX_L6_SZAIPI015468-35_2.fq
jellyfish histo -o BV143_21mer_out.histo BV143_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BV155_21mer_out --min-qual-char=? 121019_I248_FCC19K0ACXX_L6_SZAIPI015469-37_1.fq 121019_I248_FCC19K0ACXX_L6_SZAIPI015469-37_2.fq
jellyfish histo -o BV155_21mer_out.histo BV155_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BH1477_21mer_out --min-qual-char=? 121105_I263_FCD1DC2ACXX_L2_SZAIPI016297-123_1.fq 121105_I263_FCD1DC2ACXX_L2_SZAIPI016297-123_2.fq
jellyfish histo -o BH1477_21mer_out.histo BH1477_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BH1697_21mer_out --min-qual-char=? 121105_I263_FCD1DC2ACXX_L2_SZAIPI016315-136_1.fq 121105_I263_FCD1DC2ACXX_L2_SZAIPI016315-136_2.fq
jellyfish histo -o BH1697_21mer_out.histo BH1697_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BH1371_21mer_out --min-qual-char=? 121105_I263_FCD1DC2ACXX_L5_SZAIPI016314-166_1.fq 121105_I263_FCD1DC2ACXX_L5_SZAIPI016314-166_2.fq
jellyfish histo -o BH1371_21mer_out.histo BH1371_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BH582_21mer_out --min-qual-char=? FCC2GVKACXX-SZAIPI034184-57_1.fq FCC2GVKACXX-SZAIPI034184-57_2.fq
jellyfish histo -o BH582_21mer_out.histo BH582_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BH762_21mer_out --min-qual-char=? FCC2GVKACXX-SZAIPI034185-64_1.fq FCC2GVKACXX-SZAIPI034185-64_2.fq
jellyfish histo -o BH762_21mer_out.histo BH762_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BH840_21mer_out --min-qual-char=? FCC2GVKACXX-SZAIPI034186-66_1.fq FCC2GVKACXX-SZAIPI034186-66_2.fq
jellyfish histo -o BH840_21mer_out.histo BH840_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BH1655_21mer_out --min-qual-char=? FCC2HCMACXX-SZAIPI034187-74_L3_1.fq FCC2HCMACXX-SZAIPI034187-74_L3_2.fq
jellyfish histo -o BH1655_21mer_out.histo BH1655_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o BH1438_21mer_out --min-qual-char=? FCD2H1HACXX-SZAIPI034188-75_L8_1.fq FCD2H1HACXX-SZAIPI034188-75_L8_2.fq
jellyfish histo -o BH1438_21mer_out.histo BH1438_21mer_out

