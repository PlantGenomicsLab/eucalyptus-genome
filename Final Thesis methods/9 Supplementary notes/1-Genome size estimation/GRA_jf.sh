#!/bin/bash
#PBS -l nodes=1:ppn=28
#PBS -l walltime=72:00:00
#PBS -q bigmem
#PBS -o /nlustre/users/anneri/genome_size/WGS_data/E_grandis/jellyfish.o
#PBS -e /nlustre/users/anneri/genome_size/WGS_data/E_grandis/jellyfish.e
#PBS -k oe
#PBS -m ae
#PBS -M anneri.lotter@fabi.up.ac.za


module load jellyfish-2.3.0
cd /nlustre/users/anneri/genome_size/WGS_data/E_grandis/
for f in *.tar.gz; do tar xf "$f"; done

jellyfish count -t 28 -C -m 21 -s 100G -o AP926_21mer_out --min-qual-char=? ./121103_I248_FCC1934ACXX_L5_SZAIPI016110-74_1.fq ./121103_I248_FCC1934ACXX_L5_SZAIPI016110-74_2.fq
jellyfish histo -o AP926_21mer_out.histo AP926_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP925_21mer_out --min-qual-char=? ./121103_I248_FCC1934ACXX_L5_SZAIPI016128-57_1.fq ./121103_I248_FCC1934ACXX_L5_SZAIPI016128-57_2.fq
jellyfish histo -o AP925_21mer_out.histo AP925_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP928_21mer_out --min-qual-char=? ./121103_I248_FCC1934ACXX_L6_SZAIPI016086-75_1.fq ./121103_I248_FCC1934ACXX_L6_SZAIPI016086-75_2.fq
jellyfish histo -o AP928_21mer_out.histo AP928_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP927_21mer_out --min-qual-char=? ./121103_I248_FCC1934ACXX_L6_SZAIPI016131-62_1.fq ./121103_I248_FCC1934ACXX_L6_SZAIPI016131-62_2.fq
jellyfish histo -o AP927_21mer_out.histo AP927_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP929_21mer_out --min-qual-char=? ./121103_I248_FCC1934ACXX_L7_SZAIPI016087-89_1.fq ./121103_I248_FCC1934ACXX_L7_SZAIPI016087-89_2.fq
jellyfish histo -o AP929_21mer_out.histo AP929_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP924_21mer_out --min-qual-char=? ./121103_I248_FCC1934ACXX_L8_SZAIPI016085-88_1.fq ./121103_I248_FCC1934ACXX_L8_SZAIPI016085-88_2.fq
jellyfish histo -o AP924_21mer_out.histo AP924_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP921_21mer_out --min-qual-char=? ./121104_I235_FCC18BLACXX_L2_SZAIPI016082-47_1.fq ./121104_I235_FCC18BLACXX_L2_SZAIPI016082-47_2.fq
jellyfish histo -o AP921_21mer_out.histo AP921_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP922_21mer_out --min-qual-char=? ./121104_I235_FCC18BLACXX_L3_SZAIPI016133-111_1.fq ./121104_I235_FCC18BLACXX_L3_SZAIPI016133-111_2.fq
jellyfish histo -o AP922_21mer_out.histo AP922_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP923_21mer_out --min-qual-char=? ./121104_I235_FCC18BLACXX_L4_SZAIPI016134-56_1.fq ./121104_I235_FCC18BLACXX_L4_SZAIPI016134-56_2.fq
jellyfish histo -o AP923_21mer_out.histo AP923_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP931_21mer_out --min-qual-char=? ./121105_I263_FCC18KVACXX_L5_SZAIPI016307-129_1.fq ./121105_I263_FCC18KVACXX_L5_SZAIPI016307-129_2.fq
jellyfish histo -o AP931_21mer_out.histo AP931_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP930_21mer_out --min-qual-char=? ./121105_I263_FCC18KVACXX_L5_SZAIPI016312-79_1.fq ./121105_I263_FCC18KVACXX_L5_SZAIPI016312-79_2.fq
jellyfish histo -o AP930_21mer_out.histo AP930_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP933_21mer_out --min-qual-char=? ./121105_I263_FCC18KVACXX_L6_SZAIPI016295-142_1.fq ./121105_I263_FCC18KVACXX_L6_SZAIPI016295-142_2.fq
jellyfish histo -o AP933_21mer_out.histo AP933_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP932_21mer_out --min-qual-char=? ./121105_I263_FCC18KVACXX_L6_SZAIPI016308-90_1.fq ./121105_I263_FCC18KVACXX_L6_SZAIPI016308-90_2.fq
jellyfish histo -o AP932_21mer_out.histo AP932_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP934_21mer_out --min-qual-char=? ./121105_I263_FCD1DC2ACXX_L1_SZAIPI016296-93_1.fq ./121105_I263_FCD1DC2ACXX_L1_SZAIPI016296-93_2.fq
jellyfish histo -o AP934_21mer_out.histo AP934_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP937_21mer_out --min-qual-char=? ./121105_I263_FCD1DC2ACXX_L1_SZAIPI016305-169_1.fq ./121105_I263_FCD1DC2ACXX_L1_SZAIPI016305-169_2.fq
jellyfish histo -o AP937_21mer_out.histo AP937_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP939_21mer_out --min-qual-char=? ./121105_I263_FCD1DC2ACXX_L3_SZAIPI016293-94_1.fq ./121105_I263_FCD1DC2ACXX_L3_SZAIPI016293-94_2.fq
jellyfish histo -o AP939_21mer_out.histo AP939_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP959_21mer_out --min-qual-char=? ./AP959_1.fq ./AP959_2.fq
jellyfish histo -o AP959_21mer_out.histo AP959_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP960_21mer_out --min-qual-char=? ./AP960_1.fq ./AP960_2.fq
jellyfish histo -o AP960_21mer_out.histo AP960_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP962_21mer_out --min-qual-char=? ./AP962_FCD20EGACXX_L3_SZAIPI026231-105_1.fq ./AP962_FCD20EGACXX_L3_SZAIPI026231-105_2.fq
jellyfish histo -o AP962_21mer_out.histo AP962_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP964_21mer_out --min-qual-char=? ./AP964_FCD20EGACXX_L3_SZAIPI026232-106_1.fq ./AP964_FCD20EGACXX_L3_SZAIPI026232-106_2.fq
jellyfish histo -o AP964_21mer_out.histo AP964_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP965_21mer_out --min-qual-char=? ./AP965_FCD20EGACXX_L4_SZAIPI026233-107_1.fq ./AP965_FCD20EGACXX_L4_SZAIPI026233-107_2.fq
jellyfish histo -o AP965_21mer_out.histo AP965_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP966_21mer_out --min-qual-char=? ./AP966_FCD20EGACXX_L4_SZAIPI026234-108_1.fq ./AP966_FCD20EGACXX_L4_SZAIPI026234-108_2.fq
jellyfish histo -o AP966_21mer_out.histo AP966_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP967_21mer_out --min-qual-char=? ./AP967_1.fq ./AP967_2.fq
jellyfish histo -o AP967_21mer_out.histo AP967_21mer_out

jellyfish count -t 28 -C -m 21 -s 100G -o AP968_21mer_out --min-qual-char=? ./AP968_1.fq ./AP968_2.fq
jellyfish histo -o AP968_21mer_out.histo AP968_21mer_out
