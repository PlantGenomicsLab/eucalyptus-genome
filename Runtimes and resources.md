| Analysis | Software | Input(s) Size | Parameters | Memory Given | Cores Given | Memory Efficiency | CPU Efficiency | Job Completed Successfully | Recommended Resources | Notes | 
| --- | --- |  --- | ---  | --- | --- | --- | --- | --- | --- | --- |
|   Removal of contaminant reads |   Kraken2 | File size > 28 Gb zipped  |   -db /isg/shared/databases/kraken2/Standard --paired --use-names --threads 8 --output filename.out --unclassified-out file_unclassified#.fastq --classified-out file_classified#.fastq --report report.txt -use-mpa-style    |   200G    |   8   |   19% |   74% |   Yes |   NA |    6h  |
|   Removal of contaminant reads    |   Centrifuge  |   File size > 129G    |   -x p+h+v --report-file report.tsv --quiet --min-hitlen 100 -q file.fastq    |   250G    |   1   |   6%  |   50% |   Yes |   NA  | 11h    |
|   Genome Assembly |   Flye    |   Raw-ONT fastq 129G  |   --nano-raw --genome-size 650m --out-dir --threads 36    |   500 G   |   36  |  NA   |   NA  |   Yes |   NA  |   1 - 1.5 d   |
|   |   Falcon  |   ONT fasta converted file  64G   |   NA  |   100G    |   1   |   3%  |   1.5%    |   Yes |   NA  |   2 - 3 d |
|   |   Shasta  |   ONT fasta converted file 64G    |   --Reads.minReadLength 500 --memoryMode anonymous --memoryBacking 4K --threads 16    |   450G    |   16  |   75% |   60% |   Yes |   NA  |   5h    |    
|   |   wtdbg2  |   ONT fastq file  | -x ont -g 650m -t 24 -e 2 |   NA  |   28  |   NA  |   NA  |   Yes |   NA  |   NA  |
|   |   Masurca - short reads   |   Unclassified fastq files > 23G  |   config.txt file |   500G    |   32  |   14% |   67% |   Yes |   NA  |   1.5 - 2d    |
|   |   Masurca - hybrid    |   unclassified short reads and long reads |   config.txt file |   500G    |   32  |   43% |   91% |   Yes |   Same    |   10 - 11d    |
|   Remove redundant reads  |   PurgeHaplotigs  | bam file of raw reads to genome you want to purge    |   NA   |   450G    |   16  |   NA |    |   Yes |   NA  |   1h  |
|   Canu binning    |   Canu v1.8   |   raw fastq long reads    |   -p  -d genomeSize=650m -minReadLength=1000 -useGrid=True -haplotypeURO -haplotypeGRA -nanopore-raw  |   500G    |   32  |   NA  |   NA  |   Yes |   NA  |   NA  |
|   |   Masurca - binned  |   config.txt file | unclassified fastq files and binned reads   |  400G    |   28  |   61% |   96% |   Yes |   NA  |   10 - 11d    |
|   Read based contig check |   Polar_star  |   fasta file of assembly and binned reads |   "threads"     : "20", "opts" : "-x map-ont -a", "name", "lib", "low_depth": "2", "times_mean" : "3" |   300G    |   20  |   5%  |   50% |   Yes |   NA  |   1 - 2h  |
|   Scaffolding with genetic linkage maps   |   blast   | fasta file, SNP probes    |   -evalue 0.00001 -outfmt 6 -num_alignments 1 -max_hsps 1 |      80G  |   16  |   NA  |   24% |   Yes |   NA  |   < 5min  |
|   |   ALLMAPS |   fasta file, genetic maps    |   NA  |   150G    |   16  |   1.10%   |   25% |   Yes |   NA  |   8 min  |


*Note general custom scripts written in python, R, perl, etc are NOT multi-threaded (unless you specifically made them so). Please use 1 core for executing these scripts.
The memory used for these scripts should depend on the size of the input.
*CPU Efficiency is a misleading metric as it also includes the time the job spends in the queue.
